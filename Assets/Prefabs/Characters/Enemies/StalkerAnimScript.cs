﻿using UnityEngine;
using System.Collections;

public class StalkerAnimScript : MonoBehaviour {
    NetworkManager netman;

	Animator enemyAnim;

    Transform player;

	internal bool idle,standingDSAttack, doubleLRAttack, runLSAttack, runRSAttack, leftLRAttack, RightLRAttack;

    internal bool isNearPlayer;

	// Use this for initialization
	void Start () {
		enemyAnim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
        States();
        animationTransition();

        
	}

    void animationTransition()
    {
        float fb = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.z;
        float lr = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.x;

        if (fb != 0||lr != 0)
        {
            idle = false;
            standingDSAttack = false;

            if(enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Standing Double Slash Attack"))
            {
                standingDSAttack = false;
            }

            if (!(enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Running Right Slash Attack") && enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Running Left Slash Attack")))
            {
                runLSAttack = true;

            }

            if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Running Right Slash Attack"))
            {
                if(enemyAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                {
                    runRSAttack = false;
                    runLSAttack = true;
                }
            }
            if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Running Left Slash Attack"))
            {
                if (enemyAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                {
                    runRSAttack = true;
                    runLSAttack = false;
                }
            }



        }
        else
        {
            if (isNearPlayer)
            {
                runLSAttack = false;
                runRSAttack = false;
                idle = false;

                standingDSAttack = true;
            }
            else
            {
                runLSAttack = true;
                standingDSAttack = false;
            }
        }

        //if (!PhotonNetwork.isMasterClient)
        //{
        //    if (enemyAnim.GetCurrentAnimatorStateInfo(0).IsName("Standing Double Slash Attack"))
        //    {
        //        if (enemyAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
        //        {
        //            if (GetComponent<NetworkedAI>().currentTarget != null)
        //            {
        //                if (GetComponent<NetworkedAI>().currentTarget.GetComponent<MovementScript>() != null)
        //                {
        //                    if (GetComponent<NetworkedAI>().currentTarget.GetComponent<PhotonView>().photonView.isMine)
        //                    {
        //                        GetComponent<NetworkedAI>().currentTarget.GetComponent<MovementScript>().playerHP -= 1;
        //                    }
        //                }

        //            }
        //            else
        //            {
        //                enemyAnim.SetBool("Standing Double Slash Attack", false);
        //                enemyAnim.SetBool("Idle", true);
        //            }
        //        }
        //    }
        //}
    }

    void States()
    {
        enemyAnim.SetBool("Standing Double Slash Attack", standingDSAttack);
        enemyAnim.SetBool("Double Long Range Attack", doubleLRAttack);
        enemyAnim.SetBool("Run Left Slash Attack", runLSAttack);
        enemyAnim.SetBool("Run Right Slash Attack", runRSAttack);
        enemyAnim.SetBool("Left Long Range Attack", leftLRAttack);
        enemyAnim.SetBool("Right Long Range Attack", RightLRAttack);
       
    }
}
