﻿using UnityEngine;
using System.Collections;

public class BruteAnimScript : MonoBehaviour
{

	Animator enemyAnim;

	public bool running,guard, runBash, runBash2, attack,standingAtk,idle;
    public bool isNearPlayer;
	// Use this for initialization
	void Start ()
	{
		enemyAnim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
        animationTransition();
	}

    void animationTransition()
    {
        float fb = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.z;
        float lr = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.x;

        if(fb !=0 || lr != 0)
        {
            enemyAnim.SetBool("Running Bash", true);
        }

        if (isNearPlayer)
        {
            enemyAnim.SetBool("Running Bash", false);
            enemyAnim.SetBool("Running Bash 2", true);
        }
        else
        {
            enemyAnim.SetBool("Running Bash", true);
            enemyAnim.SetBool("Running Bash 2", false);
        }
    }

    void States()
    {
        //enemyAnim.SetBool("Running", running);
        //enemyAnim.SetBool("Guard", guard);
        //enemyAnim.SetBool("Running Bash", runBash);
        //enemyAnim.SetBool("Running Bash 2", runBash2);
        //enemyAnim.SetBool("Attack", attack);

    }
}
