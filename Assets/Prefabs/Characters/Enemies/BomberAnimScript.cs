﻿using UnityEngine;
using System.Collections;

public class BomberAnimScript : MonoBehaviour {
    Animator enemyAnim;

    public bool isNearPlayer;
	// Use this for initialization
	void Start () {
        enemyAnim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        animationTransition();
	}

    void animationTransition()
    {
        float fb = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.z;
        float lr = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.x;

        if (fb != 0 || lr != 0)
        {
            enemyAnim.SetBool("Running", true);
        }

        if (isNearPlayer)
        {
            enemyAnim.SetBool("Running", false);
            enemyAnim.SetBool("Explode", true);
        }
        else
        {
            enemyAnim.SetBool("Running", true);
            enemyAnim.SetBool("Explode", false);
        }

    }
}
