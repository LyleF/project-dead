﻿using UnityEngine;
using System.Collections;

public class BMAnimScript : MonoBehaviour
{

	Animator playerAnim;

	bool doWallRun, leftWall, rightWall;

	// Use this for initialization
	void Start ()
	{
		doWallRun = false; // false if you're not doing a wall run for running speed purposes...

		// to play the left or right wall run...
		leftWall = false;
		rightWall = false;
		playerAnim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float fb = Input.GetAxis ("Vertical");
		float lr = Input.GetAxis ("Horizontal");

		playerAnim.SetFloat ("FB", fb * .5f);
		playerAnim.SetFloat ("LR", lr * .5f);

		if (Input.GetKey (KeyCode.LeftShift)) {
			if (!doWallRun) {
				playerAnim.speed = 2;
			}

			if (doWallRun) {
				if (leftWall) {
					playerAnim.SetBool ("LWRun", true);
				}

				if (rightWall) {
					playerAnim.SetBool ("RWRun", true);
				}
			}
		} else {
			playerAnim.SetBool ("LWRun", false);
			playerAnim.SetBool ("RWRun", false);
			playerAnim.speed = 1.5f;
		}
	}
}
