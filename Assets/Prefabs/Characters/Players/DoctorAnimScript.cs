﻿using UnityEngine;
using System.Collections;

public class DoctorAnimScript : MonoBehaviour {

    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float fb = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.z;
        float lr = GetComponent<UnityEngine.AI.NavMeshAgent>().velocity.x;

        GetComponent<Animator>().SetFloat("move", Mathf.Abs(fb * .5f));
        GetComponent<Animator>().SetFloat("move", Mathf.Abs(lr * .5f));

        if (fb == 0 && lr == 0)
        {
            if (GetComponent<NetworkedAI>().currentTarget != gameObject)
            {
                GetComponent<Animator>().SetBool("beingRescued", true);
            }

            if (GetComponent<NetworkedAI>().networkSurvivorRescued)
            {
                GetComponent<Animator>().SetBool("beingRescued", false);
            }
        }
	}
}
