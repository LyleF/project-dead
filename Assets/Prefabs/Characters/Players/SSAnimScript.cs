﻿using UnityEngine;
using System.Collections;

public class SSAnimScript : MonoBehaviour
{
    MovementScript movementAnims;
    Animator playerAnim;

	bool doWallRun, leftWall, rightWall;

	// Use this for initialization
	void Start ()
	{
		doWallRun = false; // false if you're not doing a wall run for running speed purposes...

		// to play the left or right wall run...
		leftWall = false;
		rightWall = false;
		playerAnim = GetComponent<Animator> ();
        movementAnims = GetComponentInParent<MovementScript>();
    }
	
	// Update is called once per frame
	void Update ()
	{
		float fb = Input.GetAxis ("Vertical");
		float lr = Input.GetAxis ("Horizontal");

        playerAnim.SetFloat("FB", fb * .5f);
        playerAnim.SetFloat("LR", lr * .5f);

        if (Input.GetKey(KeyCode.LeftShift) && fb > 0)
        {
            if (!movementAnims.isWallRunningLeft && !movementAnims.isWallRunningRight)
            {
                playerAnim.speed = 2;
            }

            if (movementAnims.isWallRunningLeft)
            {
                playerAnim.SetBool("LWRun", true);
            }
            else
            {
                playerAnim.SetBool("LWRun", false);
            }

            if (movementAnims.isWallRunningRight)
            {
                playerAnim.SetBool("RWRun", true);
            }
            else
            {
                playerAnim.SetBool("RWRun", false);
            }

        }
        else
        {
            playerAnim.SetBool("LWRun", false);
            playerAnim.SetBool("RWRun", false);
            playerAnim.speed = 1.5f;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            playerAnim.SetBool("Reload", true);
        }
        else
        {
            playerAnim.SetBool("Reload", false);
        }
    }
}
