﻿using UnityEngine;
using System.Collections;

public class L86Anims : MonoBehaviour {
    Animator playerAnims;
    bool checkReload;
    bool isPlayingReload;
	// Use this for initialization
	void Start () {
        playerAnims = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        playerAnims.SetBool("shoot",false);

        if (Input.GetKey(KeyCode.Mouse0) && GetComponent<KrissVectorScript>().MagazineCapacity != 0)
        {
            playerAnims.SetBool("shoot", true);
        }
        else
            playerAnims.SetBool("shoot", false);

        if (Input.GetKeyDown(KeyCode.R))
        {
            playerAnims.SetBool("reload",true);
        }
        else
        {
            playerAnims.SetBool("reload", false);
        }

        if (playerAnims.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            if(playerAnims.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                GetComponent<KrissVectorScript>().Reload();
            }
        }
	}
}
