﻿using UnityEngine;
using System.Collections;

public class BM_MacheteScrpt : MonoBehaviour {

	Animator BMMacheteAnimator;

	// Use this for initialization
	void Start () {

		BMMacheteAnimator = GetComponent<Animator> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		BMMacheteAnimator.SetBool ("Attack1", false);
		BMMacheteAnimator.SetBool ("Attack2", false);
		BMMacheteAnimator.SetBool ("Attack3", false);

		//Walk Front and Back
		float ForwardBackward = Input.GetAxis ("Vertical");
		BMMacheteAnimator.SetFloat ("RFRB", -ForwardBackward);

		//Walk Left and Right
		float LeftRight = Input.GetAxis ("Horizontal");
		BMMacheteAnimator.SetFloat ("RLRR", LeftRight);

		//Attack1
		bool checkAttack1 = false;
		if (Input.GetButtonDown ("Fire1")) {

			checkAttack1 = true;
			BMMacheteAnimator.SetBool ("Attack1", checkAttack1);

		} 

		//Attack2
		bool checkAttack2 = false;
		if (Input.GetButtonDown ("Fire2")) {

			checkAttack2 = true;
			BMMacheteAnimator.SetBool ("Attack2", checkAttack2);

		} 

		//Attack3
		bool checkAttack3 = false;
		if (Input.GetKeyDown(KeyCode.H)) {

			checkAttack3 = true;
			BMMacheteAnimator.SetBool ("Attack3", checkAttack3);

		} 

		//Hook
		bool checkHook = false;
		//if (Input.GetKeyDown(KeyCode.V)) {

		//	checkHook = true;
		//	BMMacheteAnimator.SetBool ("Hook", checkHook);

		//} 
	
	}
}
