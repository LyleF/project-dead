﻿using UnityEngine;
using System.Collections;

public class SS_Gol_Animation : MonoBehaviour {

	Animator SSGolAnimator;

	// Use this for initialization
	void Start () {

		SSGolAnimator = GetComponent<Animator>();
	
	}
	
	// Update is called once per frame
	void Update () {

		SSGolAnimator.SetBool ("Fire", false);

		//Walk Front and Back
		float ForwardBackward = Input.GetAxis ("Vertical");
		SSGolAnimator.SetFloat ("RFRB", -ForwardBackward);

		//Walk Left and Right
		float LeftRight = Input.GetAxis ("Horizontal");
		SSGolAnimator.SetFloat ("RLRR", LeftRight);

		//Fire
		bool checkFire = false;
		if (Input.GetButtonDown ("Fire1")) {

			checkFire = true;
			SSGolAnimator.SetBool ("Fire", checkFire);

		} 

		//Full reload
		bool checkReload = false;
        if (GetComponent<KrissVectorScript>().MagazineCapacity < GetComponent<KrissVectorScript>().InitialMagCap)
        {
            if (Input.GetKeyDown(KeyCode.R) && !SSGolAnimator.GetCurrentAnimatorStateInfo(0).IsName("FullReload") || GetComponent<KrissVectorScript>().MagazineCapacity == 0)
            {

                SSGolAnimator.SetBool("FullReload", true);

            }
        }
        else
        if (GetComponent<KrissVectorScript>().MagazineCapacity > 0)
        {

            SSGolAnimator.SetBool("FullReload", false);
        }
	
	}
}
