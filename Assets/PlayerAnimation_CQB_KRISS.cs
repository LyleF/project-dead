﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerAnimation_CQB_KRISS : Photon.MonoBehaviour {

	public Animator playerAnimator;
    public bool checkReload;
    public bool isplayingReload;

    private GameObject myImage;
    // Use this for initialization
    void Start () {
        if (photonView.isMine)
        {
            myImage = GameObject.Find("CQB_Skill");
            playerAnimator = GetComponent<Animator>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (photonView.isMine)
        {
            playerAnimator.SetBool("Fire", false);
            playerAnimator.SetBool("RapidFire", false);

            //Walk Front and Back
            float ForwardBackward = Input.GetAxis("Vertical");
            playerAnimator.SetFloat("RFRB", -ForwardBackward);

            //Walk Left and Right
            float LeftRight = Input.GetAxis("Horizontal");
            playerAnimator.SetFloat("RLRR", LeftRight);

            //Fire
            bool checkFire = false;
            bool checkRapidFire = false;
            if (Input.GetButtonDown("Fire1") && GetComponent<KrissVectorScript>().MagazineCapacity != 0)
            {

                checkFire = true;
                checkRapidFire = false;
                playerAnimator.SetBool("Fire", checkFire);

            }
            else if (Input.GetButton("Fire1") && GetComponent<KrissVectorScript>().MagazineCapacity != 0)
            {

                checkRapidFire = true;
                checkFire = false;
                playerAnimator.SetBool("RapidFire", checkRapidFire);
            }



            //Full reload
            if (Input.GetKeyDown(KeyCode.R) && GetComponent<KrissVectorScript>().MagazineCapacity != GetComponent<KrissVectorScript>().InitialMagCap)
            {

                checkReload = true;
                playerAnimator.SetBool("FullReload", checkReload);

            }
            else if (Input.GetKeyUp(KeyCode.R))
            {

                checkReload = false;
                playerAnimator.SetBool("FullReload", checkReload);
            }
            //   Debug.Log("im reloading" + playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("FullReload"));
            if (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Full reload"))
            {
                if (playerAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                {
                    GetComponent<KrissVectorScript>().Reload();
                }
            }
            //Grappling hook
            bool checkHook = false;
            if (Input.GetKey(KeyCode.V) && transform.root.GetComponent<GrapplingHook>().allowedToFire)
            {
                if (myImage.GetComponent<Image>().fillAmount == 1)
                {
                    checkHook = true;
                    if (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Grappling hook idle"))
                        transform.root.GetComponent<GrapplingHook>().fireHook = true;
                    playerAnimator.SetBool("Hook", checkHook);
                }
            }
            else
            {
                checkHook = false;
                transform.root.GetComponent<GrapplingHook>().fireHook = false;
                playerAnimator.SetBool("Hook", checkHook);
            }

            if (myImage.GetComponent<Image>().fillAmount < 1)
                myImage.GetComponent<Image>().fillAmount += 0.5f * Time.deltaTime;
            else
                if (myImage.GetComponent<Image>().fillAmount == 1)
            {
                myImage.GetComponent<Image>().color = new Color(255, 255, 255);
            }
        }
    }
}
