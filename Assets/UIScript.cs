﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

	public Sprite cqb, sharpshooter, medic, blademaster, bioticslab, morayta, ruralarea;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CQB(){
		GameObject.Find ("CharacterDisplay").GetComponent<Image> ().sprite = cqb;
		GameObject.Find ("Title").GetComponent<Text> ().text = "CQB";
		GameObject.Find ("Descriptions").GetComponent<Text> ().text = "Specializes in shotguns and submachine guns";
	}

	public void Sharpshooter(){
		GameObject.Find ("CharacterDisplay").GetComponent<Image> ().sprite = sharpshooter;

		GameObject.Find ("Title").GetComponent<Text> ().text = "Sharpshooter";
		GameObject.Find ("Descriptions").GetComponent<Text> ().text = "Specializes in sniper rifles and pistols";
	}

	public void Medic(){
		GameObject.Find ("CharacterDisplay").GetComponent<Image> ().sprite = medic;

		GameObject.Find ("Title").GetComponent<Text> ().text = "Medic";
		GameObject.Find ("Descriptions").GetComponent<Text> ().text = "Specializes in light support weapons and healing";
	}

	public void Blademaster(){
		GameObject.Find ("CharacterDisplay").GetComponent<Image> ().sprite = blademaster;

		GameObject.Find ("Title").GetComponent<Text> ().text = "Blademaster";
		GameObject.Find ("Descriptions").GetComponent<Text> ().text = "Specializes in bladed weapons and melee combat";
	}

	public void BioticsLab(){
		GameObject.Find ("Overview").GetComponent<Image> ().sprite = bioticslab;

        GameObject.Find("LocationDefinition").GetComponent<Text>().text = "Biotics Lab";
	}

	public void Morayta(){
		GameObject.Find ("Overview").GetComponent<Image> ().sprite = morayta;

        GameObject.Find("LocationDefinition").GetComponent<Text>().text = "Morayta";
	}

	public void RuralArea(){
		GameObject.Find ("Overview").GetComponent<Image> ().sprite = ruralarea;

        GameObject.Find("LocationDefinition").GetComponent<Text>().text = "Rural Area";
	}

	public void Exit () {
		Application.Quit ();
	}
}
