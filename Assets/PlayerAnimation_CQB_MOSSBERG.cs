﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerAnimation_CQB_MOSSBERG : MonoBehaviour {

	Animator playerAnimator;

	// Use this for initialization
	void Start () {

		playerAnimator = GetComponent<Animator> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		playerAnimator.SetBool ("Fire", false);


		//Walk Front and Back
		float ForwardBackward = Input.GetAxis ("Vertical");
		playerAnimator.SetFloat ("RFRB", -ForwardBackward);

		//Walk Left and Right
		float LeftRight = Input.GetAxis ("Horizontal");
		playerAnimator.SetFloat ("RLRR", LeftRight);


		//Fire
		bool checkFire = false;
		if (Input.GetButtonDown ("Fire1")) {

			checkFire = true;
			playerAnimator.SetBool ("Fire", checkFire);

		} 

		//Full reload
		bool checkReload = false;
		if (Input.GetKeyDown (KeyCode.R) && playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("FullReload")) {

			checkReload = true;
			playerAnimator.SetBool ("FullReload", checkReload);

		} else {

			checkReload = false;
			playerAnimator.SetBool ("FullReload", checkReload);
		}

		//Grappling hook
		bool checkHook = false;
		if (Input.GetKeyDown (KeyCode.V)) {

			checkHook = true;
			playerAnimator.SetBool ("Hook", checkHook);

		} 
	}
}
