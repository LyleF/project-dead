﻿using UnityEngine;
using System.Collections;

public class WristAttacks : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.transform.tag == "Player" )
        {
            if (transform.root.name.Contains("Stalker"))
            {
                col.transform.GetComponent<MovementScript>().playerHP -= 5;
                col.GetComponent<MovementScript>().isHit = true;
            }
            if (transform.root.name.Contains("Brute"))
            {
                col.transform.GetComponent<MovementScript>().playerHP -= 10;
                col.GetComponent<MovementScript>().isHit = true;
            }
        }

       
    }
}
