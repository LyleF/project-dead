﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIControlTest : MonoBehaviour {
    private Camera thisCam;

    private int AIType;
    private int targetIndex;

    private List<GameObject> friendlyAI;
    private List<GameObject> enemyAI;


	void Start () {
        friendlyAI = new List<GameObject>();
        enemyAI = new List<GameObject>();

        friendlyAI.AddRange(GameObject.FindGameObjectsWithTag("Survivor"));
        enemyAI.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));

        foreach(GameObject fAI in friendlyAI)
        {
            fAI.GetComponent<MedicAnimScript>().AIControlled = true;
        }

        foreach(GameObject eAI in enemyAI)
        {
            eAI.GetComponent<Renderer>().material.color = Color.red;
        }
        targetIndex = Random.Range(0, friendlyAI.Count);

        AIType = 1;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            AIType = 1; //friendly
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            AIType = 2;
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            setAIPosition(ray);
        }

        chaseSurvivor();
	}

    void setAIPosition(Ray ray)
    {
        RaycastHit hit;

        if (AIType == 1)
        {
            foreach (GameObject friend in friendlyAI)
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    friend.GetComponent<AIBehavior>().Destination = hit.point;
                }
            }
        }

        if(AIType == 2)
        {

        }
    }

    void chaseSurvivor()
    {

        foreach (GameObject eAI in enemyAI)
        {
            foreach (GameObject fAI in friendlyAI)
            {
                eAI.GetComponent<AIBehavior>().Destination = friendlyAI[targetIndex].transform.position;
            }
        }

    }

    void attackAI(Ray ray)
    {

    }
}
