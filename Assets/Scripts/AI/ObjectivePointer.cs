﻿using UnityEngine;
using System.Collections;

public class ObjectivePointer : MonoBehaviour {
    private GameObject player;
    private Transform objectiveArea;
	// Use this for initialization
	void Start () {
        objectiveArea = GameObject.Find("RESCUE_AREA").transform;

        foreach (Transform currentTransform in transform)
        {
            if (currentTransform.GetComponentInParent<PlayerInteraction>() != null)
            {
                Debug.Log("Found parent!");
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(objectiveArea);
	}

    public Transform setObjective
    {
        get { return objectiveArea; }
        set { objectiveArea = value; }
    }
}
