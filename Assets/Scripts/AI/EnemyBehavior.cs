﻿using UnityEngine;
using System.Collections;

public class EnemyBehavior : MonoBehaviour {
    UnityEngine.AI.NavMeshAgent agent;
	// Use this for initialization
	void Start () {
      agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.isMasterClient)
        {
            MoveAI();
        }
    }

    void MoveAI()
    {
        agent.destination = GameObject.Find("target").transform.position;
    }
}
