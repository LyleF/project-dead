﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class PlayerInteraction : Photon.MonoBehaviour {
    private List<GameObject> friendlyAI;
    private List<GameObject> currentlyFollowingAI;

    private List<GameObject> enemyAI;
    private GameObject tempAI;

    private NetworkManager netMan;
    private bool sendFollowRequest;

    private int networkedID;

    void Awake()
    {
        currentlyFollowingAI = new List<GameObject>();
        netMan = FindObjectOfType<NetworkManager>();
    }
    // Use this for initialization
    void Start () {
        friendlyAI = new List<GameObject>();
        enemyAI = new List<GameObject>();

        friendlyAI.AddRange(GameObject.FindGameObjectsWithTag("Survivor"));
        enemyAI.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
    }
	
	// Update is called once per frame
	void Update () {
        if (photonView.isMine)
        {
        
            if (Input.GetKeyDown(KeyCode.E))
            {
                photonView.RPC("followPlayer",PhotonTargets.AllBuffered);

                Debug.Log("Current thing's ID:" + photonView.viewID);
            }


            if (checkRescued() != null)
            {
                if (checkRescued().GetComponent<AIBehavior>().survivorRescued)
                {
                    if (GetComponentInChildren<ObjectivePointer>() != null)
                    {
                        GetComponentInChildren<ObjectivePointer>().setObjective = GameObject.Find("RESCUE_AREA").transform;
                        currentlyFollowingAI.Clear();
                    }
                }
            }
        }
	}
    [PunRPC]
    void followPlayer()
    {
        friendlyAI.AddRange(GameObject.FindGameObjectsWithTag("Survivor"));

        foreach (GameObject friend in friendlyAI)
        {
            if (Vector3.Distance(transform.position,friend.transform.position) < 5)
            {
                friend.GetComponent<PhotonView>().TransferOwnership(photonView.viewID);

                if (currentlyFollowingAI.Count < 2)
                {

                    currentlyFollowingAI.Add(friend);
                    friend.GetComponent<NetworkedAI>().currentTarget = gameObject;
                    tempAI = friend;

                }
            }

            if (friend.GetComponent<AIBehavior>().survivorRescued)
            {
                currentlyFollowingAI.Remove(friend);
            }
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(photonView.viewID);
        }
        else
            if (stream.isReading)
        {
            networkedID = (int)stream.ReceiveNext();
        }
    }

    public GameObject checkRescued()
    {
        if (tempAI != null)
            return tempAI;
        else
            return null;
    }
}
