﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AIBehavior : Photon.MonoBehaviour {
    private UnityEngine.AI.NavMeshAgent agent;

    private StalkerAnimScript StalkerAnims;
    private BruteAnimScript BruteAnims;
    private BomberAnimScript bomberAnims;

    private NetworkManager netMan;

   internal Vector3 destination;
    private List<GameObject> players;
    internal GameObject playerTarget;

    private int numberOfAttacks;
    private int AiType;

    bool decrementList;
    #region RESCUE_VARS
    private Transform safeArea;

    private bool isSafe;
    #endregion
    // Use this for initialization

    void Awake()
    {
        players = new List<GameObject>();

        if(transform.tag == "Survivor")
        {
            AIType = 0;
        }else
            if(transform.tag == "Enemy")
        {
            AIType = 1;
        }
    }
    void Start()
    {
        if(transform.name.Contains("Stalker"))
            StalkerAnims = GetComponent<StalkerAnimScript>();
        if (transform.name.Contains("Brute"))
            BruteAnims = GetComponent<BruteAnimScript>();
        if (transform.name.Contains("Bomber"))
            bomberAnims = GetComponent<BomberAnimScript>();

            agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

   
            Destination = transform.position;
            netMan = FindObjectOfType<NetworkManager>();

            players.AddRange(GameObject.FindGameObjectsWithTag("Player"));

            if (AIType == 0)
            {
                safeArea = GameObject.Find("EXTRACTION_AREA").transform;
            }
            if (AIType == 1)
            {
                playerTarget = players[0];
            }
        
    }

    // Update is called once per frame
    void Update()
    {

        //moveHere();
        if (PhotonNetwork.isMasterClient)
        {
            goToPlayer();
            rescueMethod();
        }
    }

    void moveHere()
    {
        agent.destination = destination;
    }
    
    void goToPlayer()
    {
        if(GetComponent<NetworkedAI>().currentTarget!=null && PhotonNetwork.isMasterClient)
        {
            if (AIType == 0)
            {
                if (Vector3.Distance(transform.position, GetComponent<NetworkedAI>().currentTarget.transform.position) > 5 && !isSafe)
                {
                    agent.destination = GetComponent<NetworkedAI>().currentTarget.transform.position;
                }
                else
                {
                    agent.destination = transform.position;
                }
            }

            if (AIType == 1)
            {

                if (transform.name.Contains("Stalker")) {
                    if (Vector3.Distance(transform.position, GetComponent<NetworkedAI>().currentTarget.transform.position) > 4f && GetComponent<NetworkedAI>().currentTarget.tag != "Enemy")
                    {
                        agent.destination = GetComponent<NetworkedAI>().currentTarget.transform.position;

                            StalkerAnims.isNearPlayer = false;
                    }
                    else
                    {
                        attackState();
                    }
                }

                if (transform.name.Contains("Brute"))
                {
                    if (Vector3.Distance(transform.position, GetComponent<NetworkedAI>().currentTarget.transform.position) < 50f)
                    {
                        agent.speed = 15f;
                    }
                    if (Vector3.Distance(transform.position, GetComponent<NetworkedAI>().currentTarget.transform.position) > 3f && GetComponent<NetworkedAI>().currentTarget.tag != "Enemy")
                    {
                        agent.destination = GetComponent<NetworkedAI>().currentTarget.transform.position;
                        BruteAnims.isNearPlayer = false;

                    }
                    else
                        attackState();
                }

                if (transform.name.Contains("Bomber"))
                {
                    if (Vector3.Distance(transform.position, GetComponent<NetworkedAI>().currentTarget.transform.position) > 5f && GetComponent<NetworkedAI>().currentTarget.tag != "Enemy")
                    {
                        agent.destination = GetComponent<NetworkedAI>().currentTarget.transform.position;
                        bomberAnims.isNearPlayer = false;

                    }
                    else
                    {

                        photonView.RPC("Suicide", PhotonTargets.All);
                    }
                }
            }
        }
    }

    #region enemyMethods
    void attackState()
    {
        RaycastHit forwardRay;

       // transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, (GetComponent<NetworkedAI>().currentTarget.transform.position - transform.position), 10 * Time.deltaTime, 0.0f));
        agent.destination = transform.position;

        if (transform.name.Contains("Stalker"))
        {
            StalkerAnims.isNearPlayer = true;

            if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Standing Double Slash Attack"))
            {
                if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f && GetComponent<NetworkedAI>().currentTarget != null)
                {
                    Debug.Log("attack");
                }
                else
                if(netMan.GetComponent<NetworkManager>().inGamePlayers.Count>0)
                {
                  //  GetComponent<NetworkedAI>().currentTarget = netMan.GetComponent<NetworkManager>().inGamePlayers[Random.Range(0, netMan.GetComponent<NetworkManager>().inGamePlayers.Count)];
                    GetComponent<Animator>().SetBool("Standing Double Slash Attack", false);
                    StalkerAnims.standingDSAttack = false;
                    StalkerAnims.isNearPlayer = false;
                }
            }
        }
        if (transform.name.Contains("Brute"))
        {
            BruteAnims.isNearPlayer = true;

            if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Running Bash"))
            {
                if(netMan.GetComponent<NetworkManager>().inGamePlayers.Count > 0)
                {
                    GetComponent<NetworkedAI>().currentTarget = netMan.GetComponent<NetworkManager>().inGamePlayers[Random.Range(0, netMan.GetComponent<NetworkManager>().inGamePlayers.Count)];
                    GetComponent<Animator>().SetBool("Running Bash", false);
                }
            }
        } 
    }
    #endregion

    #region SurvivorMethods
    void rescueMethod()
    {
        if (AIType == 0)
        {
            if (Vector3.Distance(transform.position, safeArea.transform.position) < 10)
            {
                isSafe = true;
            }
        }
    }
    #endregion
    public GameObject PlayerTarget
    {
        get { return playerTarget; }
        set { playerTarget = value; }
    }
    public Vector3 Destination
    {
        get { return destination; }
        set { destination = value; }
    }

    public bool survivorRescued
    {
        get { return isSafe; }
        set { isSafe = value; }
    }
    public int AIType
    {
        get { return AiType; }
        set { AiType = value; }
    }
    public int timesAttacked
    {
        get { return numberOfAttacks; }
        set { numberOfAttacks = value; }
    }

    [PunRPC]
    void Suicide()
    {
        agent.destination = transform.position;
        GetComponent<NetworkedAI>().currentTarget = gameObject;

        bomberAnims.isNearPlayer = true;
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Explode"))
        {
            GameObject fart = PhotonNetwork.Instantiate("redSmoke",transform.position,Quaternion.identity,0,null);
            PhotonNetwork.Instantiate("BloodSplat", transform.position, Quaternion.identity, 0, null);
            if (!decrementList)
            {
                GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("DecrementEnemyCount", PhotonTargets.All);
                decrementList = true;
            }
            Destroy(gameObject);
        }
    }
    [PunRPC]
    void fart()
    {
            if (!decrementList)
            {
                GameObject fart = PhotonNetwork.Instantiate("redSmoke", transform.position, Quaternion.identity, 0, null);
                PhotonNetwork.Instantiate("BloodSplat", transform.position, Quaternion.identity, 0, null);
               // GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("DecrementEnemyCount", PhotonTargets.All);
                decrementList = true;
            
            }
          
    }
}
