﻿using UnityEngine;
using System.Collections;

public class LimbHP :Photon.MonoBehaviour {

    private float timer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	   
	}
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "PlayerProjectile")
        {
            if (transform.root.GetComponent<EnemyHP>().hp > 0)
            {
                GetComponent<Rigidbody>().AddForce(transform.InverseTransformDirection(col.transform.position) * 500f);
                transform.root.GetComponent<EnemyHP>().photonView.RPC("takingDamage", PhotonTargets.MasterClient,col.gameObject.GetComponent<PlayerProjectile>().damage);
            }
            
         //   GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.forward)  *  50000f);
          //  GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.up) * 50000f);
            // GetComponent<Rigidbody>().AddForce(Vector3.up * 35000);
        }
    }
}
