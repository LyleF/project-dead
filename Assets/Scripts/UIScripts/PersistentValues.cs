﻿using UnityEngine;
using System.Collections;

public class PersistentValues : MonoBehaviour {
	public static PersistentValues persistentValues;

	private string serverName;

	private int Difficulty;
	private int WaveCount;
    private int gameMode;

	private bool isCQB;
	private bool isSharpshooter;
	private bool isMedic;
	private bool isBladeMaster;

	private bool isOnline;

	// Use this for initialization
	void Start () {
		persistentValues=this;
        serverName = null;
	}

    void Update()
    {
    
    }

	public void setNetworkOfflineState(bool value){
		isOnline=value;
	}
	public void setNetworkOnlineState(bool value){
		isOnline=value;
	}
    public void setGameMode(int gameMode)
    {
        this.gameMode = gameMode;
    }
    public int getGameMode()
    {
        return gameMode;
    }
	public bool getNetworkOnlineState(){
		return isOnline;
	}
	public void setServerName(string name){
		serverName=name;
	}
	public string getServerName(){
		return serverName;
	}
	public void setDifficulty(int difficulty){
		Difficulty=difficulty;
	}
	public int getDifficulty(){
		return Difficulty;
	}
	public void setWaves(int waves){
		WaveCount=waves;
	}
	public int getWaves(){
		return WaveCount;
	}

	public void setCQBClass(bool val){
		isCQB=val;	
	}
	public bool getCQBClass(){
		return isCQB;
	}
	public void setSharp(bool val){
		isSharpshooter=val;
	}
	public bool getSharp(){
		return isSharpshooter;
	}
	public void setBM(bool val){
		isBladeMaster=val;
	}
	public bool getBM(){
		return isBladeMaster;
	}
	public void setMedic(bool val){
		isMedic=val;
	}
	public bool getMedic(){
		return isMedic;
	}
}	
