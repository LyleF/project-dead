﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class MainMenu_Buttons : MonoBehaviour {

	public static MainMenu_Buttons Menu;

    public Camera myCam;

	public Sprite clickedOnline;
	public Sprite clickedOffline;

	private Sprite unclickedOnline;
	private Sprite unclickedOffline;

	public Sprite clickedRescue;
	public Sprite clickedSurvival;

	private Sprite UnclickedRescue;
	private Sprite UnclickedSurvival;

	public Sprite clickedEndless;
	public Sprite clickedWave;

	private Sprite UnclickedEndless;
	private Sprite UnclickedWave;

	private bool _clickedOnline;
	private bool _clickedOffline;

	private bool _clickedRescue;
	private bool _clickedSurvival;

	private bool _clickedendless;
	private bool _clickedWave;

	private bool _clickedBladeMaster;
	private bool _clickedSharpshooter;
	private bool _clickedCQB;
	private bool _clickedMedic;

	private int difficulty;
	private int waveAmount;

	private string stringWave;
	private string levelName;

	private GameObject MainPanel;

	private GameObject PlayButton;
	private GameObject OptionButton;
	private GameObject ClassButton;
	private GameObject ExitButton;

	private GameObject[] Panels;

	private GameObject PlayPanel;
	private GameObject RescueButton;
	private GameObject SurvivalButton;
	private GameObject EndlessButton;
	private GameObject WaveButton;
	private GameObject OnlineButton;
	private GameObject OfflineButton;

	private GameObject ClassPanel;

	private GameObject OptionsPanel;

	private GameObject RoomCreationPanel;
	private GameObject OfflineStart;
	private GameObject OfflineCancel;
	private GameObject WaveInput;	
	private GameObject WaveText;
	private GameObject serverText;
	private GameObject DifficultyText;
	private GameObject ServerInput;

	private GameObject Diff_Easy;
	private GameObject Diff_Average;
	private GameObject Diff_Hard;

	private GameObject notice;
	private GameObject Return;

    private GameObject sampleClassRestingSpot;

    private GameObject CQBPlayer;
    private GameObject SniperPlayer;
    private GameObject MedicPlayer;
    private GameObject BladePlayer;

	private GameObject persistentValue;

    private AsyncOperation loader;
	// Use this for initialization
	void Start () {
		Menu=this;

		Panels = new GameObject[10];

		for(int i=0;i<10;i++){
			Panels[i]=GameObject.Find("PanelSpace_"+(i+1));
		}
		MainPanel=GameObject.Find("MainMenu");

		PlayButton=GameObject.Find("PlayButton");
		ClassButton=GameObject.Find("ClassesButton");
	//	OptionButton=GameObject.Find("OptionsButton");
		ExitButton=GameObject.Find("ExitButton");

		PlayPanel=GameObject.Find("PlayPanel");
		RescueButton=GameObject.Find("RescueButton");
		SurvivalButton=GameObject.Find("SurvivalButton");
		WaveButton=GameObject.Find("WaveBasedButton");
		EndlessButton=GameObject.Find("EndlessButton");
		OnlineButton=GameObject.Find("OnlineButton");
		OfflineButton=GameObject.Find("OfflineButton");

		RoomCreationPanel=GameObject.Find("RoomCreationOptions");
		OfflineStart=GameObject.Find("Start");
		OfflineCancel=GameObject.Find("Cancel");
		WaveInput=GameObject.Find("WavesInputField");
		WaveText=GameObject.Find("WaveCount");
		DifficultyText=GameObject.Find("DifficultyText");
		ServerInput=GameObject.Find("ServerName");
		serverText=GameObject.Find("server");

		Diff_Easy=GameObject.Find("Ez");
		Diff_Average=GameObject.Find("Med");
		Diff_Hard=GameObject.Find("Hard");

		ClassPanel=GameObject.Find("ClassesPanel");
		Return=GameObject.Find("Return");

		notice=GameObject.Find("Warning");

        CQBPlayer = GameObject.Find("CQB_Player_SAMPLE");
        SniperPlayer = GameObject.Find("Sharpshooter_Player_SAMPLE");
        MedicPlayer = GameObject.Find("Medic_Player_SAMPLE");
        BladePlayer = GameObject.Find("BladeMaster_Player_SAMPLE");

        sampleClassRestingSpot = GameObject.Find("RestSpot");

		_clickedCQB=true;

		difficulty=1;
			
		persistentValue=GameObject.Find("PersistentValues");

        persistentValue.GetComponent<PersistentValues>().setCQBClass(true);
        persistentValue.GetComponent<PersistentValues>().setGameMode(1);
        persistentValue.GetComponent<PersistentValues>().setDifficulty(2);
	}
    void Update()
    {
        if (loader != null)
        {
            if (!loader.isDone)
            {
                Debug.Log("Still loading"+loader.progress);
            }
            else
                Debug.Log("done");
        }
    }
	public void PlayMenu(){
		for(int i=0;i<5;i++){
			Panels[i].SetActive(true);
		}
		PlayPanel.SetActive(true);

		EndlessButton.SetActive(false);
		WaveButton.SetActive(false);
		notice.SetActive(false);

		RoomCreationPanel.SetActive(false);
		MainPanel.SetActive(false);
	}
	public void OnClickedRescue(){

            persistentValue.GetComponent<PersistentValues>().setGameMode(1);

	}
	public void OnClickedSurvival(){

            persistentValue.GetComponent<PersistentValues>().setGameMode(2);

	}

    public void OnClickedSurvivalWave()
    {
        persistentValue.GetComponent<PersistentValues>().setGameMode(3);
    }
	public void OnClickedEndless(){
		if(!_clickedendless){
			EndlessButton.GetComponent<Image>().sprite=clickedEndless;
			WaveButton.GetComponent<Image>().sprite=UnclickedWave;

			_clickedendless=true;
			_clickedWave=false;
		}
	}
	public void OnClickedWave(){
		if(!_clickedWave){
			WaveButton.GetComponent<Image>().sprite=clickedWave;
			EndlessButton.GetComponent<Image>().sprite=UnclickedEndless;

			_clickedWave=true;
			_clickedendless=false;
		}
	}
	public void OnClickedOnline(){

			_clickedOnline=true;
			_clickedOffline=false;


			persistentValue.GetComponent<PersistentValues>().setNetworkOnlineState(true);
			//persistentValue.GetComponent<PersistentValues>().setNetworkOfflineState(false);
	}
	public void OnClickedOffline(){
			_clickedOnline=false;
			_clickedOffline=true;


			//persistentValue.GetComponent<PersistentValues>().setNetworkOfflineState(true);
			persistentValue.GetComponent<PersistentValues>().setNetworkOnlineState(false);
	}
	public void ClassMenu(){
		for(int i=0;i<5;i++){
			Panels[i].SetActive(true);
		}

		MainPanel.SetActive(false);

		ClassPanel.SetActive(true);
	}
	public void OnClickedSharpshooter(){
		_clickedSharpshooter=true;
		_clickedMedic=false;
		_clickedCQB=false;
		_clickedBladeMaster=false;

		persistentValue.GetComponent<PersistentValues>().setSharp(true);
		persistentValue.GetComponent<PersistentValues>().setMedic(false);
		persistentValue.GetComponent<PersistentValues>().setCQBClass(false);
		persistentValue.GetComponent<PersistentValues>().setBM(false);
	}
	public void OnClickedMedic(){
		_clickedSharpshooter=false;
		_clickedMedic=true;
		_clickedCQB=false;
		_clickedBladeMaster=false;

		persistentValue.GetComponent<PersistentValues>().setSharp(false);
		persistentValue.GetComponent<PersistentValues>().setMedic(true);
		persistentValue.GetComponent<PersistentValues>().setCQBClass(false);
		persistentValue.GetComponent<PersistentValues>().setBM(false);
	}
	public void OnClickCQB(){
		_clickedSharpshooter=false;
		_clickedMedic=false;
		_clickedCQB=true;
		_clickedBladeMaster=false;

		persistentValue.GetComponent<PersistentValues>().setSharp(false);
		persistentValue.GetComponent<PersistentValues>().setMedic(false);
		persistentValue.GetComponent<PersistentValues>().setCQBClass(true);
		persistentValue.GetComponent<PersistentValues>().setBM(false);
	}
	public void OnClickBladeMaster(){
		_clickedSharpshooter=false;
		_clickedMedic=false;
		_clickedCQB=false;
		_clickedBladeMaster=true;


		persistentValue.GetComponent<PersistentValues>().setSharp(false);
		persistentValue.GetComponent<PersistentValues>().setMedic(false);
		persistentValue.GetComponent<PersistentValues>().setCQBClass(false);
		persistentValue.GetComponent<PersistentValues>().setBM(true);
	}
	public void OnClickClass(){
		MainPanel.SetActive(true);
		ClassPanel.SetActive(false);

		for(int i=0;i<5;i++){
			Panels[i].SetActive(false);
		}
	}

	public void GoBack(){
		for(int i=0;i<5;i++){
			Panels[i].SetActive(false);
		}
		PlayPanel.SetActive(false);
		MainPanel.SetActive(true);
	}
	public void CancelOffline(){
		for(int i=5;i<10;i++){
			Panels[i].SetActive(false);
		}
		RoomCreationPanel.SetActive(false);
	}
	public void PlayGame(){
		if((_clickedOffline)&&(_clickedSurvival&&_clickedWave||_clickedRescue)){
			RoomCreationPanel.SetActive(true);

			for(int i=5;i<10;i++){
				Panels[i].SetActive(true);
			}

			if(_clickedSurvival){
				if(_clickedWave){
					WaveText.SetActive(true);
					WaveInput.SetActive(true);

					serverText.SetActive(false);
					ServerInput.SetActive(false);
				}
			}else
				if(_clickedRescue){
					WaveText.SetActive(false);
					WaveInput.SetActive(false);

					serverText.SetActive(false);
					ServerInput.SetActive(false);
				}
		}else
			if(_clickedendless){
				StartGame();
			}else
				if(_clickedOnline){
					RoomCreationPanel.SetActive(true);

					WaveText.SetActive(false);
					WaveInput.SetActive(false);


					DifficultyText.SetActive(false);
					Diff_Easy.SetActive(false);
					Diff_Average.SetActive(false);
					Diff_Hard.SetActive(false);


			}
	}
	public void NoticeMethod(){
		notice.SetActive(false);
	}

    
	public void StartGame(){
        if (persistentValue.GetComponent<PersistentValues>().getNetworkOnlineState() == true)
        {
            if (persistentValue.GetComponent<PersistentValues>().getServerName() != null)
            {
                DontDestroyOnLoad(persistentValue);
                loader = Application.LoadLevelAsync(levelName);
                
         //       loader = SceneManager.LoadSceneAsync();
            }
            else
                GameObject.Find("Placeholder").GetComponent<Text>().text = "Empty Input!";
        }else
        {
            DontDestroyOnLoad(persistentValue);
            loader = Application.LoadLevelAsync(levelName);
        }
	}
	public void setLevelNameToBiotics(){
		levelName="BioticsLab";
	}
	public void setLevelNameToRural(){
		levelName="RuralMap";
	}
    public void setLevelNameToMorayta()
    {
        levelName = "Morayta";
    }

	public void Options(){
		
	}
    public void easyDiff()
    {
        persistentValue.GetComponent<PersistentValues>().setDifficulty(1);
        
    }
    public void medDiff()
    {
        persistentValue.GetComponent<PersistentValues>().setDifficulty(2);
    }

    public void hardDiff()
    {
        persistentValue.GetComponent<PersistentValues>().setDifficulty(3);
    }

    public void enableSound()
    {
        AudioListener.pause = false;
    }

    public void disableSound()
    {
        AudioListener.pause = true;
    }

    public void low()
    {
        QualitySettings.SetQualityLevel(0);
    }
    
    public void med()
    {
        QualitySettings.SetQualityLevel(3);
    }

    public void high()
    {
        QualitySettings.SetQualityLevel(5);
    }
	public void getWaves(){
		stringWave=WaveInput.GetComponent<InputField>().text;
		int.TryParse(stringWave,out waveAmount);
        persistentValue.GetComponent<PersistentValues>().setWaves(waveAmount);
	}
	public void getServer(){
        ServerInput = GameObject.Find("ServerName");

        persistentValue.GetComponent<PersistentValues>().setServerName(ServerInput.GetComponent<InputField>().text);
	}
	public void Exit(){
		Application.Quit();
	}
}
