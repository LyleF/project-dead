﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerUI : Photon.MonoBehaviour {
	private GameObject PersistentHolder;

	private GameObject CQBPanel;
	private GameObject SharpPanel;
	private GameObject BladePanel;
	private GameObject MedicPanel;

	private GameObject HPBar;
	private GameObject ArmorBar;
	private GameObject StaminaBar;

    private GameObject resumeBtn;
    private GameObject leaveBtn;
	private GameObject PausePanel;
    void Awake()
    {
        PersistentHolder = GameObject.Find("PersistentValues");

        if (PersistentHolder.GetComponent<PersistentValues>().getNetworkOnlineState() == false)
        {
            CQBPanel = GameObject.Find("CQB_Panel");
            SharpPanel = GameObject.Find("Sharpshooter_Panel");
            BladePanel = GameObject.Find("Blademaster_Panel");
            MedicPanel = GameObject.Find("Medic_Panel");
        }
    }
	// Use this for initialization
	void Start () {
        if (PersistentHolder.GetComponent<PersistentValues>().getNetworkOnlineState() == true)
        {
            CQBPanel = GameObject.Find("CQB_Panel");
            SharpPanel = GameObject.Find("Sharpshooter_Panel");
            BladePanel = GameObject.Find("Blademaster_Panel");
            MedicPanel = GameObject.Find("Medic_Panel");
        }
        HPBar =GameObject.Find("HPText");
		ArmorBar=GameObject.Find("ArmorText");
		StaminaBar=GameObject.Find("StaminaText");

        PausePanel = GameObject.Find("PauseMenu");
        resumeBtn = GameObject.Find("ResumeBtn");
        leaveBtn = GameObject.Find("MenuBtn");

        Debug.Log(PersistentHolder.GetComponent<PersistentValues>().getBM()+" my hp:");

        //resumeBtn.GetComponent<Button>().onClick.AddListener(() => { Resume(); });
        //leaveBtn.GetComponent<Button>().onClick.AddListener(() => { Exit(); });

        if (transform.tag != "Player")
        {
            setClass();
        }
    
     //   PausePanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.tag != "Player")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;

                if (!PausePanel.activeInHierarchy)
                    PausePanel.SetActive(true);
                else
                    if (PausePanel.activeInHierarchy)
                    PausePanel.SetActive(false);
            }
        }
        if (transform.name != "Handler")
        {
            ModifyHP();
            ModifyStamina();
        }
    }
	private void ModifyStamina(){
        if (GameObject.Find("YourPlayer") != null)
            StaminaBar.GetComponent<Text>().text=Mathf.Round(GameObject.Find("YourPlayer").GetComponent<MovementScript>().stamina).ToString();
	}
    private void ModifyHP()
    {
        if(GameObject.Find("YourPlayer")!=null)
        HPBar.GetComponent<Text>().text = Mathf.Round(GameObject.Find("YourPlayer").GetComponent<MovementScript>().playerHP).ToString();
    }
	private void setClass(){
		if(PersistentHolder.GetComponent<PersistentValues>().getBM()){
			BladePanel.SetActive(true);
			CQBPanel.SetActive(false);
			SharpPanel.SetActive(false);
			MedicPanel.SetActive(false);
		}else
			if(PersistentHolder.GetComponent<PersistentValues>().getCQBClass()){
				CQBPanel.SetActive(true);
				BladePanel.SetActive(false);
				SharpPanel.SetActive(false);
				MedicPanel.SetActive(false);
			}else
				if(PersistentHolder.GetComponent<PersistentValues>().getSharp()){
					CQBPanel.SetActive(false);
					BladePanel.SetActive(false);
					SharpPanel.SetActive(true);
					MedicPanel.SetActive(false);
				}else
					if(PersistentHolder.GetComponent<PersistentValues>().getMedic()){
						CQBPanel.SetActive(false);
						BladePanel.SetActive(false);
						SharpPanel.SetActive(false);
						MedicPanel.SetActive(true);
			}
		}
	public void Resume(){
		GameObject.Find("PauseMenu").SetActive(false);
	}
	public void Exit(){
		PhotonNetwork.Disconnect();
		PhotonNetwork.LeaveRoom();
	
		Application.LoadLevel("Project Dead UI");
	}
//	private void setGameMode(){	
//	
//	}
}
