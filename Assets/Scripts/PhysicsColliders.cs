﻿using UnityEngine;
using System.Collections;

public class PhysicsColliders : MonoBehaviour {
	private Transform Pos;

	private float playerZ;
	// Use this for initialization
	void Start () {
		Pos=GameObject.Find("CarryPoint").transform;
	}
	
	// Update is called once per frame
	void Update () {

	

	}
	void OnCollisionEnter(Collision col){
		playerZ=Pos.position.z;
		if(col.gameObject.name!="Player"&&playerZ>0){
			playerZ-=0.5f;
		}
		else{
			playerZ=2;
		}
			Pos.position = new Vector3(Pos.position.x,Pos.position.y,playerZ);
	}
}
