﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Collections;   

public class NetworkManager : Photon.MonoBehaviour {

    private const string Version = "v4.2";

    public string playerPrefabName = "CQB_Player";
    public string AIPrefab = "SphereAI";
    public string RoomName;

    private GameObject AIBall;
    private GameObject persistentValues;

    public GameObject AISpawn;
    public GameObject[] SpawnPoint;

    public List<GameObject> enemySpawnPoints;
    public List<GameObject> survivorSpawnPoints;

    private GameObject player;

    private GameObject serverInfo_Name;
    private GameObject serverInfo_Ping;

    public List<GameObject> enemyAI;

    public List<GameObject> friendlyAI;
    public List<GameObject> inGamePlayers;

    private int spawnIndex;

    private int enemySpawnIndex;
    private int survivorSpawnIndex;

    public bool GameOver;

    public bool gameStart;

    public bool repositionEveryone;

    public int SURVIVAL_networkCurrentWave;
    public int SURVIVAL_networkMaxWave;
    public int SURVIVAL_maxEnemyCount;

    public int mySpectatorID;

    private float AISpawnDelay;

    void Awake() {

        SURVIVAL_networkCurrentWave = 1;

        friendlyAI = new List<GameObject>();
        enemyAI = new List<GameObject>();
        inGamePlayers = new List<GameObject>();

        friendlyAI.AddRange(GameObject.FindGameObjectsWithTag("Survivor"));
        enemyAI.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));

        foreach (GameObject fAI in friendlyAI)
        {
            fAI.GetComponent<MedicAnimScript>().AIControlled = true;
            fAI.GetComponent<AIBehavior>().AIType = 0; // friendly
        }
        persistentValues = GameObject.Find("PersistentValues");
    }
    void Start() {
        serverInfo_Name = GameObject.Find("ServerName");
        serverInfo_Ping = GameObject.Find("PingVal");

        //=GameObject.Find("PersistentValues");

        spawnIndex = Random.Range(0, 4);
        if (persistentValues.GetComponent<PersistentValues>().getNetworkOnlineState()) {

            PhotonNetwork.ConnectUsingSettings(Version); //compares current game version to connecting server
            Debug.Log("connected to a game");
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.autoCleanUpPlayerObjects = false;

                PhotonNetwork.isMessageQueueRunning = true;
            }

        }
        else {
            PhotonNetwork.offlineMode = true;
            PhotonNetwork.CreateRoom("Offline Session");
            Debug.Log("Offline game");
        }
        PhotonNetwork.sendRate = 45;
        PhotonNetwork.sendRateOnSerialize = 35;
    }
    #region NetworkSetup
    void OnJoinedLobby() {

        RoomOptions roomOptions = new RoomOptions() {
            MaxPlayers = 4
        };
        Debug.Log("IN LOBBY" + persistentValues.GetComponent<PersistentValues>().getServerName());
        if (persistentValues.GetComponent<PersistentValues>().getNetworkOnlineState() == true) {
            PhotonNetwork.offlineMode = false;
            PhotonNetwork.JoinOrCreateRoom(persistentValues.GetComponent<PersistentValues>().getServerName() + "_MODE_" + persistentValues.GetComponent<PersistentValues>().getGameMode() + "_DIFF_"+persistentValues.GetComponent<PersistentValues>().getDifficulty(), roomOptions, TypedLobby.Default);
            Debug.Log(PhotonNetwork.room);
        } else
            if (persistentValues.GetComponent<PersistentValues>().getNetworkOnlineState() == false) {
            PhotonNetwork.offlineMode = true;
            PhotonNetwork.CreateRoom("Offline Session");
            Debug.Log("DX");
        }


    }
    void OnJoinedRoom()
    {

            if (persistentValues.GetComponent<PersistentValues>().getSharp())
            {
                player = PhotonNetwork.Instantiate("Sharpshooter_Player",
                    SpawnPoint[spawnIndex].transform.position,
                    SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            else
                if (persistentValues.GetComponent<PersistentValues>().getCQBClass())
            {
                player = PhotonNetwork.Instantiate("CQB_Player",
                    SpawnPoint[spawnIndex].transform.position,
                    SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            else
                    if (persistentValues.GetComponent<PersistentValues>().getBM())
            {
                player = PhotonNetwork.Instantiate("BladeMaster_Player",
                    SpawnPoint[spawnIndex].transform.position,
                    SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            else
                        if (persistentValues.GetComponent<PersistentValues>().getMedic())
            {
                player = PhotonNetwork.Instantiate("Medic_Player",
                SpawnPoint[spawnIndex].transform.position,
                SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            inGamePlayers.Add(player);
   

        Debug.Log("Current mode:"+persistentValues.GetComponent<PersistentValues>().getGameMode()+" Current difficulty:"+persistentValues.GetComponent<PersistentValues>().getDifficulty());
    }
    #endregion
    void Update()
    {
        if (GameObject.Find("MyCam") != null)
        {
            Debug.Log("SPEC ID:" + mySpectatorID+" found spec id:"+ (GameObject.Find("MyCam").GetComponent<PhotonView>().viewID));
        }

        if (Input.GetKeyDown(KeyCode.Y) && GameObject.Find("MyCam") != null)
        {
            if (persistentValues.GetComponent<PersistentValues>().getSharp())
            {
                player = PhotonNetwork.Instantiate("Sharpshooter_Player",
                    SpawnPoint[spawnIndex].transform.position,
                    SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            else
       if (persistentValues.GetComponent<PersistentValues>().getCQBClass())
            {
                player = PhotonNetwork.Instantiate("CQB_Player",
                    SpawnPoint[spawnIndex].transform.position,
                    SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            else
           if (persistentValues.GetComponent<PersistentValues>().getBM())
            {
                player = PhotonNetwork.Instantiate("BladeMaster_Player",
                    SpawnPoint[spawnIndex].transform.position,
                    SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
            else
               if (persistentValues.GetComponent<PersistentValues>().getMedic())
            {
                player = PhotonNetwork.Instantiate("Medic_Player",
                SpawnPoint[spawnIndex].transform.position,
                SpawnPoint[spawnIndex].transform.rotation, 0) as GameObject;
            }
           // inGamePlayers.Add(player);
            PhotonNetwork.Destroy(PhotonView.Find(mySpectatorID).gameObject);

        }

        if (persistentValues.GetComponent<PersistentValues>().getGameMode() == 1) // Rescue
        {
            if (PhotonNetwork.isMasterClient)
            {
                if (Input.GetKeyDown(KeyCode.H) && !gameStart)
                {
                    GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("startGame",PhotonTargets.All);
                    inGamePlayers = new List<GameObject>();
                    inGamePlayers.AddRange(GameObject.FindGameObjectsWithTag("Player"));
                }

                Rescue();

                if (gameStart)
                {
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Survivors that need rescuing:" + friendlyAI.Count(x => x.GetComponent<AIBehavior>().survivorRescued == false);
                    GameObject.Find("Info_2").GetComponent<Text>().text = "Survivors Rescued:" + friendlyAI.Count(x => x.GetComponent<AIBehavior>().survivorRescued == true);
                    if (!repositionEveryone)
                    {
                        foreach (GameObject iG in inGamePlayers)
                        {
                            spawnIndex = Random.Range(0, 4);

                            iG.transform.position = SpawnPoint[spawnIndex].transform.position;
                        }
                        repositionEveryone = true;
                    }
                }else
                {
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Wait for players? Press H to start: Rescue";
                }
                
            }
            else
            if(!PhotonNetwork.isMasterClient)
            {
                friendlyAI = new List<GameObject>();

                friendlyAI.AddRange(GameObject.FindGameObjectsWithTag("Survivor"));


                if (gameStart)
                {
                    if (!repositionEveryone)
                    {
                        GameObject.Find("YourPlayer").transform.position = SpawnPoint[spawnIndex].transform.position;
                        repositionEveryone = true;
                    }

                    foreach (GameObject survivorSpawn in survivorSpawnPoints)
                    {
                        foreach (GameObject survivors in friendlyAI)
                        {
                            if (Vector3.Distance(survivorSpawn.transform.position, survivors.transform.position) < 5f)
                            {
                                survivorSpawn.GetComponent<ParticleSystem>().enableEmission = true;
                            }
                            else
                                survivorSpawn.GetComponent<ParticleSystem>().enableEmission = false;
                        }
                    }

                    GameObject.Find("Info_1").GetComponent<Text>().text = "Survivors that need rescuing:" + friendlyAI.Count(x => x.GetComponent<NetworkedAI>().networkSurvivorRescued == false);
                    GameObject.Find("Info_2").GetComponent<Text>().text = "Survivors Rescued:" + friendlyAI.Count(x => x.GetComponent<NetworkedAI>().networkSurvivorRescued == true);
                }
                else
                {
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Waiting for more players:Rescue";
                }
            }
        }else
            if (persistentValues.GetComponent<PersistentValues>().getGameMode() == 2) //Endless
        {
            if (PhotonNetwork.isMasterClient)
            {
                if (Input.GetKeyDown(KeyCode.H) && !gameStart)
                {
                    GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("startGame", PhotonTargets.All);
                    inGamePlayers = new List<GameObject>();
                    inGamePlayers.AddRange(GameObject.FindGameObjectsWithTag("Player"));
                }

                if (gameStart)
                {
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Remaining Enemies:" + SURVIVAL_maxEnemyCount;
                    GameObject.Find("Info_2").GetComponent<Text>().text = "Wave:" + SURVIVAL_networkCurrentWave;
                    if (!repositionEveryone)
                    {
                        foreach (GameObject iG in inGamePlayers)
                        {
                            spawnIndex = Random.Range(0, 4);

                            iG.transform.position = SpawnPoint[spawnIndex].transform.position;
                        }
                        GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("setMaxEnemyCount", PhotonTargets.All,(inGamePlayers.Count() * 2)+(SURVIVAL_networkCurrentWave*2)); //(player count * enemySize) + wavecount * 2
                        repositionEveryone = true;
                    }
                    Endless();
                }
                else
                {
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Wait for players? Press H to start : Endless";
                }
            }
            else
                if (!PhotonNetwork.isMasterClient)
            {
                if (gameStart)
                {
                    if (!repositionEveryone)
                    {
                        GameObject.Find("YourPlayer").transform.position = SpawnPoint[spawnIndex].transform.position;
                        repositionEveryone = true;
                    }
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Remaining Enemies:" + SURVIVAL_maxEnemyCount;
                    GameObject.Find("Info_2").GetComponent<Text>().text = "Wave:" + SURVIVAL_networkCurrentWave;
                }
                else
                {
                    GameObject.Find("Info_1").GetComponent<Text>().text = "Waiting for more players: Endless";
                }
            }
        }
    }
		
    void UpdateServerText()
    {
        serverInfo_Ping.GetComponent<Text>().text = PhotonNetwork.networkingPeer.RoundTripTime.ToString();
    }
   
    #region AIOverTheNetwork
    void setAIPosition()
    {
        
    }

    public void AiToGameObjectPosition(GameObject Player, GameObject AI)
    {

        AI.GetComponent<NetworkedAI>().currentTarget = Player;
        foreach (Transform transformInPlayer in Player.transform)
        {
            foreach (Transform ArrowTransform in transformInPlayer)
            {
                if (ArrowTransform.GetComponent<ObjectivePointer>() != null)
                {
                    ArrowTransform.GetComponent<ObjectivePointer>().setObjective = GameObject.Find("EXTRACTION_AREA").transform;
                }
            }
        }
    }
    #endregion

    #region GameModes
    void Endless()
    {
        if (!GameOver)
        {
            if(Input.GetKeyDown(KeyCode.T))
                GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("nextWave", PhotonTargets.All);

            if (Input.GetKeyDown(KeyCode.Y)||(int)AISpawnDelay%5 == 0 && SURVIVAL_maxEnemyCount > enemyAI.Count)
            {
                Debug.Log("SPAWN"+enemyAI.Count);
                if (SURVIVAL_maxEnemyCount > 0 && enemyAI.Count() < 5)
                {
                        int randomNumber = Random.Range(0, 4);

                        if (randomNumber == 1)
                        {
                            GameObject enemy = PhotonNetwork.Instantiate("Stalker_BACKUP", GameObject.Find("test").transform.position, Quaternion.identity, 0, null);
                            enemyAI.Add(enemy);
                        }
                        if(randomNumber == 2)
                        {
                            GameObject enemy = PhotonNetwork.Instantiate("Brute", GameObject.Find("test").transform.position, Quaternion.identity, 0, null);
                            enemyAI.Add(enemy);
                        }
                        if(randomNumber == 3)
                        {
                        //    GameObject enemy = PhotonNetwork.Instantiate("Bomber", GameObject.Find("test").transform.position, Quaternion.identity, 0, null);
                        //    enemyAI.Add(enemy);
                        }
                }
            }

            if(SURVIVAL_maxEnemyCount == 0)
            {
                GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("pauseGame", PhotonTargets.All);
                GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("nextWave", PhotonTargets.All);
                GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("setMaxEnemyCount", PhotonTargets.All, (inGamePlayers.Count() * 2) + (SURVIVAL_networkCurrentWave * 2)); //(player count * enemySize) + wavecount * 2
            }

            AISpawnDelay += Time.deltaTime;
            checkEnemyList();
            checkPlayerList();
        }
    }

    void RPC(PhotonView playerView)
    {

    }

    void Rescue()
    {

        if (gameStart && !GameOver)
        {
            if (!repositionEveryone)
            {
                GameObject.Find("YourPlayer").transform.position = SpawnPoint[spawnIndex].transform.position;
                repositionEveryone = true;
            }

            foreach (GameObject survivorSpawn in survivorSpawnPoints)
            {
                foreach (GameObject survivors in friendlyAI)
                {
                    if (Vector3.Distance(survivorSpawn.transform.position, survivors.transform.position) < 5f)
                    {
                        survivorSpawn.GetComponent<ParticleSystem>().enableEmission = true;
                    }
                    else
                        survivorSpawn.GetComponent<ParticleSystem>().enableEmission = false;
                }
            }
 
            GameObject.Find("Info_1").GetComponent<Text>().text = "Survivors that need rescuing:" + friendlyAI.Count(x => x.GetComponent<NetworkedAI>().networkSurvivorRescued == false);
            GameObject.Find("Info_2").GetComponent<Text>().text = "Survivors Rescued:" + friendlyAI.Count(x => x.GetComponent<NetworkedAI>().networkSurvivorRescued == true);
            
            if (Input.GetKeyDown(KeyCode.Y) || (int)AISpawnDelay % 5 == 0)
            {
                //	AIBall=PhotonNetwork.InstantiateSceneObject(AIPrefab,AISpawn.transform.position,AISpawn.transform.rotation,0,null) as GameObject;

                if (enemyAI.Count < 7 * inGamePlayers.Count)
                {
                    enemySpawnIndex = Random.Range(0, enemySpawnPoints.Count);

                  //  GameObject STALKER = PhotonNetwork.InstantiateSceneObject("Stalker_BACKUP", enemySpawnPoints[enemySpawnIndex].transform.position, Quaternion.identity, 0, null) as GameObject;
                    GameObject STALKER = PhotonNetwork.InstantiateSceneObject("Stalker_BACKUP", enemySpawnPoints[enemySpawnIndex].transform.position, Quaternion.identity, 0, null) as GameObject;
                    enemyAI.Add(STALKER);
                }
            }

            if (Input.GetKeyDown(KeyCode.U) || (int)AISpawnDelay % 30 == 0)
            {
                if (friendlyAI.Count(x => x.GetComponent<AIBehavior>().survivorRescued == false) < 1)
                {
                    survivorSpawnIndex = Random.Range(0, friendlyAI.Count);

                    int randomNumber = Random.Range(0, 3);

                    if (randomNumber == 1)
                    {
                        GameObject SURVIVOR = PhotonNetwork.InstantiateSceneObject("Doctor", survivorSpawnPoints[survivorSpawnIndex].transform.position, Quaternion.identity, 0, null) as GameObject;

                        friendlyAI.Add(SURVIVOR);

                    }
                    else
                    {
                        GameObject SURVIVOR = PhotonNetwork.Instantiate("FemaleDoctor", survivorSpawnPoints[survivorSpawnIndex].transform.position, Quaternion.identity, 0, null) as GameObject;

                        friendlyAI.Add(SURVIVOR);
                    }
                }


                //GameObject.Find("SurvivorNotRescuedInfo").GetComponent<Text>().text = "Survivors that need rescuing:" + friendlyAI.Count(x => x.GetComponent<AIBehavior>().survivorRescued == false);
                //GameObject.Find("SurvivorRescuedInfo").GetComponent<Text>().text = "Survivors Rescued:" + friendlyAI.Count(x => x.GetComponent<AIBehavior>().survivorRescued == true);
            }
            foreach (GameObject survivorSpawn in survivorSpawnPoints)
            {
                foreach (GameObject survivors in friendlyAI)
                {
                    if (Vector3.Distance(survivorSpawn.transform.position, survivors.transform.position) < 5f)
                    {
                        survivorSpawn.GetComponent<ParticleSystem>().enableEmission = true;
                    }
                    else
                        survivorSpawn.GetComponent<ParticleSystem>().enableEmission = false;
                }
            }
            AISpawnDelay += Time.deltaTime;
            checkEnemyList();
            checkPlayerList();

            Debug.Log("ingame players:" + inGamePlayers.Count);
        }
        else
        {
            GameObject.Find("Info_1").GetComponent<Text>().text = "Waiting for more players";
        }
    }

    void checkEnemyList()
    {
        foreach (GameObject e in enemyAI)
        {
            if (e == null)
            {
                enemyAI.Remove(e);
                break;
            }
            if (e.GetComponent<NetworkedAI>().NULLENEMY)
            {
                //e.GetComponent<PhotonView>().RPC("destroyThis", PhotonTargets.MasterClient);
                //enemyAI.Remove(e);
            
                Debug.Log("someone is null :(");
                break;
            }
            if (e.GetComponent<EnemyHP>().hp <= 0)
            {
                enemyAI.Remove(e);
                break;
            }
        }
    }

    void checkPlayerList()
    {
        foreach(GameObject p in inGamePlayers)
        {
            if(p == null)
            {
                inGamePlayers.Remove(p);
                break;
            }
        }
    }
    #endregion

    void OnPhotonPlayerConnected()
    {
        if (PhotonNetwork.isMasterClient)
        {
            inGamePlayers = new List<GameObject>();
            inGamePlayers.AddRange(GameObject.FindGameObjectsWithTag("Player"));
        }
        else
        {
            inGamePlayers = new List<GameObject>();
            inGamePlayers.AddRange(GameObject.FindGameObjectsWithTag("Player"));

            if (gameStart)
            {
                PhotonNetwork.Disconnect();
                Debug.Log("BYE");
            }
        }
    }

    [PunRPC]
    void destroyMyCamera()
    {
        Destroy(PhotonView.Find(mySpectatorID).gameObject);
    }

}
