﻿using UnityEngine;
using System.Collections;

public class NetworkedAnimation : Photon.MonoBehaviour {

	private Animator playerAnims;

	private Vector3 position;
	private Quaternion rotation;

	private bool isRunning;
	private float forwardBackward;
	private float leftRight;
	void OnAwake(){
		transform.gameObject.SetActive(true);
        playerAnims = GetComponent<Animator>();
    }
	void Start () {

        if (GetComponent<PhotonView>()==null){
			PhotonView view=gameObject.AddComponent(typeof(PhotonView)) as PhotonView;
		}
        playerAnims = GetComponent<Animator>();
    }
	void Update () {
		
		if(!photonView.isMine){
			transform.position = Vector3.Lerp(transform.position, position,Time.deltaTime*20f);
			transform.rotation = Quaternion.Lerp(transform.rotation,rotation,Time.deltaTime*20f);

			playerAnims.SetFloat("FB",forwardBackward);
			playerAnims.SetFloat("LR",leftRight);
			playerAnims.SetBool("Sprint",isRunning);

        }else
        {
            forwardBackward = Input.GetAxis("Vertical");
            leftRight = Input.GetAxis("Horizontal");
        }
	}
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){ //player is observing things coming from this method
		if(stream.isWriting){
			stream.SendNext(playerAnims.GetFloat("FB"));
			stream.SendNext(playerAnims.GetFloat("LR"));
			stream.SendNext(playerAnims.GetBool("Sprint"));

			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
		}else
			if(stream.isReading){
				forwardBackward=(float)stream.ReceiveNext();
				leftRight=(float)stream.ReceiveNext();
				isRunning=(bool)stream.ReceiveNext();

				position = (Vector3)stream.ReceiveNext();
				rotation = (Quaternion)stream.ReceiveNext();

			}
	}	
}
