﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NetworkPlayer : Photon.MonoBehaviour {


    private NetworkManager netman;
    [SerializeField]
    private Camera myFPSCam;
    [SerializeField]
    private Camera WepCam;

    private bool isAlive = true;

    private Vector3 position;
    private Vector3 rotation;
    private Vector3 scale;

    private GameObject persistentValues;

    private float playerSmoothing = 8f;

    GameObject pausePanel;

    void Awake() {
        Debug.Log("MY ID:" + photonView.viewID);
        persistentValues = GameObject.Find("PersistentValues");

        if (photonView.isMine)
        {
            pausePanel = GameObject.Find("PauseMenu");
            pausePanel.SetActive(false);
            gameObject.name = "YourPlayer";
            myFPSCam.enabled = true;
            WepCam.enabled = true;
            //	myTPSCam.enabled=true;
            GetComponent<MovementScript>().enabled = true;
            GetComponent<CharacterController>().enabled = true;
            GetComponent<PlayerInteraction>().enabled = true;
            myFPSCam.GetComponent<AudioListener>().enabled = true;
            //	myTPSCam.GetComponent<AudioListener>().enabled=true;
            GetComponent<Rigidbody>().useGravity = true;

            GetComponent<playerSkill>().enabled = true;

            if (persistentValues.GetComponent<PersistentValues>().getCQBClass() || persistentValues.GetComponent<PersistentValues>().getBM())
                GetComponent<GrapplingHook>().enabled = true;
            if (persistentValues.GetComponent<PersistentValues>().getSharp())
                GetComponent<DroneSpawner>().enabled = true;
            if (persistentValues.GetComponent<PersistentValues>().getMedic())
            {
                GetComponent<MedicPodSpawner>().enabled = true;

              //  classShop = GameObject.Find("ShopCanvas");

              //  classShop.SetActive(false);
            }

            if (persistentValues.GetComponent<PersistentValues>().getCQBClass())
            {
                transform.Find("CQB").GetComponent<CQBAnimScript>().enabled = true;
                transform.Find("CQB").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");


                foreach (Transform obj in transform.Find("CQB"))
                {
                    obj.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                    foreach (Transform objj in obj)
                    {
                        objj.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                    }
                }
                myFPSCam.gameObject.transform.Find("Kriss").GetComponent<KrissVectorScript>().enabled = true;
            }

            if (persistentValues.GetComponent<PersistentValues>().getSharp())
            {
                transform.Find("SS").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");

                foreach (Transform obj in transform.Find("SS"))
                {
                    obj.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                    foreach (Transform objj in obj)
                    {
                        objj.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                    }
                }

                myFPSCam.gameObject.transform.Find("SS Hand Gol Magnum").GetComponent<KrissVectorScript>().enabled = true;
            }

            if (persistentValues.GetComponent<PersistentValues>().getBM())
            {
                transform.Find("BM").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");

                foreach (Transform obj in transform.Find("BM"))
                {
                    obj.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                    foreach (Transform objj in obj)
                    {
                        objj.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                        foreach (Transform blade in objj)
                        {
                            blade.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                            foreach (Transform bl in blade)
                            {
                                bl.gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                            }
                        }
                    }
                }
            }

            if (persistentValues.GetComponent<PersistentValues>().getMedic())
            {
                transform.Find("Medic").gameObject.transform.Find("Medic").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                transform.Find("Medic").gameObject.transform.Find("polySurface1").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                transform.Find("Medic").gameObject.transform.Find("polySurface12").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");
                transform.Find("Medic").gameObject.transform.Find("polySurface47").gameObject.layer = LayerMask.NameToLayer("NetworkedPlayer");


                myFPSCam.gameObject.transform.Find("L86LSW").GetComponent<KrissVectorScript>().enabled = true;
                myFPSCam.gameObject.transform.Find("M27IAR").GetComponent<KrissVectorScript>().enabled = true;
                myFPSCam.gameObject.transform.Find("M240B").GetComponent<KrissVectorScript>().enabled = true;
            }
            //	myTPSCam.enabled=false;
        }
        else
            if (!photonView.isMine)
        {

            gameObject.name = "NetworkPlayer";

            if (myFPSCam.gameObject.transform.Find("Kriss") != null)
            {
                myFPSCam.gameObject.transform.Find("Kriss").gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                foreach (Transform obj in myFPSCam.gameObject.transform.Find("Kriss"))
                {
                    obj.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                    foreach (Transform objWithinObj in obj)
                    {
                        objWithinObj.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                        foreach (Transform CombineTheMeshesPlease in objWithinObj)
                        {
                            CombineTheMeshesPlease.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                            foreach (Transform thisNeverEnds in CombineTheMeshesPlease)
                            {
                                thisNeverEnds.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                                foreach (Transform kr in thisNeverEnds)
                                {
                                    kr.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                                    foreach (Transform iss in kr)
                                    {
                                        iss.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (myFPSCam.gameObject.transform.Find("SS Hand Gol Magnum") != null)
            {
                myFPSCam.gameObject.transform.Find("SS Hand Gol Magnum").gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                foreach (Transform obj in myFPSCam.gameObject.transform.Find("SS Hand Gol Magnum"))
                {
                    obj.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                    foreach (Transform sni in obj)
                    {
                        sni.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                        foreach (Transform per in sni)
                        {
                            per.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                            foreach (Transform s in per)
                            {
                                s.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                                foreach (Transform p in s)
                                {
                                    p.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                                }
                            }
                        }
                    }
                }
            }

            if (myFPSCam.gameObject.transform.Find("BM Hand Long Blade") != null)
            {
                myFPSCam.gameObject.transform.Find("BM Hand Long Blade").gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                foreach (Transform obj in myFPSCam.gameObject.transform.Find("BM Hand Long Blade"))
                {
                    obj.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                    foreach (Transform sni in obj)
                    {
                        sni.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                        foreach (Transform per in sni)
                        {
                            per.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                            foreach (Transform s in per)
                            {
                                s.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");

                                foreach (Transform p in s)
                                {
                                    p.gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                                }
                            }
                        }
                    }
                }
            }

            if (myFPSCam.gameObject.transform.Find("L86LSW") != null)
            {
                myFPSCam.gameObject.transform.Find("L86LSW").transform.Find("polySurface62").gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                myFPSCam.gameObject.transform.Find("L86LSW").transform.Find("polySurface112").gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
                //   myFPSCam.gameObject.transform.FindChild("L86LSW").transform.FindChild("polySurface62").gameObject.layer = LayerMask.NameToLayer("ThirdPersonWeapons");
            }
            //myFPSCam.gameObject.transform.FindChild();    
            StartCoroutine("Alive");
        }

    }
    void Start() {

        if (PhotonNetwork.isMasterClient)
        {
    
            netman = FindObjectOfType<NetworkManager>();

        }
        else
        {
            netman = FindObjectOfType<NetworkManager>();
        }

    }
    void Update() {
        if (PhotonNetwork.isMasterClient)
        {
  
            netman = FindObjectOfType<NetworkManager>();
           // GetComponent<Movesc>
        }
        else
        {
            netman = FindObjectOfType<NetworkManager>();
        }

        if (photonView.isMine)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !pausePanel.activeInHierarchy)
            {
                pausePanel.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                GetComponent<MovementScript>().playerHP = 0;
            }

            if (GetComponent<MovementScript>().playerHP <= 0)
            {
                GameObject mySpecCam = PhotonNetwork.Instantiate("SpectatorCamera", transform.position, Quaternion.identity, 0, null) as GameObject;
                mySpecCam.name = "MyCam";
                netman.GetComponent<NetworkManager>().mySpectatorID = mySpecCam.GetPhotonView().viewID;

                mySpecCam.GetComponent<Movement1_1>().enabled = true;
                mySpecCam.GetComponent<Camera>().enabled = true;
                mySpecCam.GetComponent<AudioListener>().enabled = true;

                PhotonNetwork.Destroy(gameObject);
            }
        }

    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) { //player is observing things coming from this method
        if (stream.isWriting) {
            stream.SendNext(transform.position);
            stream.SendNext(transform.eulerAngles);
            //   stream.SendNext(netman.GetComponent<NetworkManager>().gameStart);
            //    stream.SendNext(netman.GetComponent<NetworkManager>().spawnEnemy);
        } else
            if (stream.isReading)
        {
            position = (Vector3)stream.ReceiveNext();
            rotation = (Vector3)stream.ReceiveNext();
            // netman.GetComponent<NetworkManager>().gameStart = (bool)stream.ReceiveNext();
            //netman.GetComponent<NetworkManager>().spawnEnemy = (bool)stream.ReceiveNext();
        }
    }
    [PunRPC]
    private void startGame()
    {
        netman.GetComponent<NetworkManager>().gameStart = true;
    }
    [PunRPC]
    private void nextWave()
    {
        netman.GetComponent<NetworkManager>().SURVIVAL_networkCurrentWave += 1;
    }

    [PunRPC]
    private void setMaxWave(int value)
    {
        netman.GetComponent<NetworkManager>().SURVIVAL_networkMaxWave = value;
    }
    [PunRPC]
    private void setMaxEnemyCount(int value)
    {
        netman.GetComponent<NetworkManager>().SURVIVAL_maxEnemyCount = value;
    }
    [PunRPC]
    private void DecrementEnemyCount()
    {
        netman.GetComponent<NetworkManager>().SURVIVAL_maxEnemyCount--;
    }
    [PunRPC]
    void destroyThis()
    {
        PhotonNetwork.Destroy(gameObject);
    }

    IEnumerator Alive()
    { // episode 9 (20 mins in) research more about coroutine
        while (isAlive)
        {
            transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * playerSmoothing);
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, rotation, Time.deltaTime * playerSmoothing);
            yield return null;
        }
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {

        Destroy(PhotonView.Find(int.Parse((player.ID + "001").ToString())).gameObject);
        Debug.Log("someone left" + photonView.viewID + "player id:" + player.ID + "User id:" + int.Parse((player.ID+"001").ToString()));
    }

    //void PositionUpdate()
    //{
    //    transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * playerSmoothing);
    //    transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * playerSmoothing);
    //}
}
