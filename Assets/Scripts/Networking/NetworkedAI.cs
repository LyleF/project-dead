﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NetworkedAI : Photon.MonoBehaviour {
    private List<GameObject> playerTargets;
    private List<GameObject> survivorTargets;
   
    private Vector3 position;
    private Vector3 rotation;
    internal GameObject currentTarget;

    internal Vector3 networkDestination;
    internal int networkHP;
    internal Vector3 networkTargetPlayer;
    int targetNetworkID;
    internal bool networkSurvivorRescued;
    public bool NULLENEMY;

    PersistentValues persistence;

  
    void Awake()
    {
        //networkHP = 100;
        playerTargets = new List<GameObject>();
        survivorTargets = new List<GameObject>();

    }

    void Start () {

        if (PhotonNetwork.isMasterClient)
        {
            persistence = FindObjectOfType<PersistentValues>();
            if (transform.tag == "Enemy")
            {
                playerTargets.AddRange(GameObject.FindGameObjectsWithTag("Player"));
                survivorTargets.AddRange(GameObject.FindGameObjectsWithTag("Survivor"));

                Debug.Log("added players" + playerTargets.Count);
                currentTarget = playerTargets[Random.Range(0, playerTargets.Count)];
            }
            if (transform.tag == "Survivor")
            {
                currentTarget = gameObject;
            }
        }

        if (!PhotonNetwork.isMasterClient)
        {
            StartCoroutine("Serialize");
        }
	}
	
	// Update is called once per frame
	public void Update () {
        if (!photonView.isOwnerActive)
        {
         //   Debug.Log("daddy is gone");
        }
        if (PhotonNetwork.isMasterClient) {
            foreach(GameObject p in playerTargets)
            {
                if(p == null)
                {
                    playerTargets.Remove(p);
                    break;
                }
            }

            StopCoroutine("Serialize");

                if (transform.GetComponent<AIBehavior>().AIType == 1)
                {
                foreach (GameObject pl in playerTargets)
                {
                    if (pl != null && currentTarget!=null)
                    {
                        if (currentTarget.tag == "Enemy")
                        {
                            currentTarget = playerTargets[Random.Range(0,playerTargets.Count)];
                        }
                        if (currentTarget != null)
                        {
                            if (transform.name.Contains("Stalker"))
                            {
                                if (Vector3.Distance(transform.position, currentTarget.transform.position) > 20f)
                                {
                                    GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 20;
                                }
                                else
                                    GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 10;
                            }
                            if (transform.name.Contains("Brute"))
                            {
                                if (Vector3.Distance(transform.position, currentTarget.transform.position) > 20f)
                                {
                                    GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 15;
                                }
                                else
                                    GetComponent<UnityEngine.AI.NavMeshAgent>().speed = 12;
                            }

                            if (persistence.GetComponent<PersistentValues>().getGameMode() == 1)
                            {
                                if (Vector3.Distance(transform.position, pl.transform.position) < 7f)
                                {

                                    if (survivorTargets[0] != null)
                                    {
                                        if (survivorTargets[0].GetComponent<Animator>().GetBool("beingRescued"))
                                            currentTarget = survivorTargets[0];
                                        else
                                            currentTarget = pl;
                                    }
                                    else
                                        currentTarget = pl;
                                }
                                else
                                    currentTarget = playerTargets[Random.Range(0, playerTargets.Count)];
                            }
         
                        }
                    }
                    else
                    {
                        if (transform.name.Contains("Stalker"))
                        {
                            GetComponent<StalkerAnimScript>().standingDSAttack = false;
                            // GetComponent<Animator>().SetBool("Standing Double Slash Attack", false);
                            GetComponent<StalkerAnimScript>().idle = true;
                        }

                        if (transform.name.Contains("Brute"))
                        {
                            GetComponent<BruteAnimScript>().running = false;
                            GetComponent<BruteAnimScript>().runBash = false;

                            
                        }
                        currentTarget = gameObject;
                    }
                }
            }
        }
        //if (!PhotonNetwork.isMasterClient)
        //{
        //    if (transform.tag == "Enemy")
        //    {
        //        if (GetComponent<EnemyHP>().hp <= 0)
        //        {
        //            StopCoroutine("Serialize");
        //            GetComponent<EnemyHP>().enableDismemberment();
        //            //Destroy(gameObject, 7f);
        //        }
        //    }
        //}
	}
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (transform.tag == "Enemy")
        {
            if (stream.isWriting)
            {
                stream.SendNext(transform.position);
                stream.SendNext(transform.eulerAngles);
                stream.SendNext(currentTarget.GetComponent<PhotonView>().viewID);
                stream.SendNext(GetComponent<EnemyHP>().hp);
            }
            else
                if (stream.isReading)
            {
                position = (Vector3)stream.ReceiveNext();
                rotation = (Vector3)stream.ReceiveNext();
                targetNetworkID = (int)stream.ReceiveNext();
                networkHP = (int)stream.ReceiveNext();
            }
        }

        if(transform.tag == "Survivor")
        {
            if (stream.isWriting)
            {
                stream.SendNext(transform.position);
                stream.SendNext(transform.eulerAngles);
                stream.SendNext(currentTarget.GetComponent<PhotonView>().viewID);
                stream.SendNext(GetComponent<AIBehavior>().survivorRescued);
            }
            else
            if (stream.isReading)
            {
                position = (Vector3)stream.ReceiveNext();
                rotation = (Vector3)stream.ReceiveNext();
                targetNetworkID = (int)stream.ReceiveNext();
                networkSurvivorRescued = (bool)stream.ReceiveNext();
            }
        }
    }

    IEnumerator Serialize()
    {
        while (true)
        {
            if (transform.tag == "Enemy")
            {
                transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 8f);
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, rotation, Time.deltaTime * 8f);

                GetComponent<EnemyHP>().hp = networkHP;

                if (PhotonView.Find(targetNetworkID) == null)
                {
                    if (PhotonView.Find(int.Parse((PhotonNetwork.masterClient.ID + "001"))) == null)
                    {
                        NULLENEMY = true;
                        currentTarget = gameObject;
                    }else
                    currentTarget = PhotonView.Find(int.Parse((PhotonNetwork.masterClient.ID + "001").ToString())).gameObject;
                }
                if (PhotonView.Find(targetNetworkID) != null)
                {
                    currentTarget = PhotonView.Find(targetNetworkID).gameObject;
                    if (Vector3.Distance(transform.position, position) > 5f)
                        GetComponent<UnityEngine.AI.NavMeshAgent>().Warp(position);
                }
            }

            if (transform.tag == "Survivor")
            {
                transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 8f);
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, rotation, Time.deltaTime * 8f);

                if (PhotonView.Find(targetNetworkID) != null)
                {
                    currentTarget = PhotonView.Find(targetNetworkID).gameObject;
                    if (Vector3.Distance(transform.position, position) > 5f)
                        GetComponent<UnityEngine.AI.NavMeshAgent>().Warp(position);
                }
                else
                {
                    Debug.Log("NULL FINDINGS" + photonView.viewID);
                    NULLENEMY = true;
                   // photonView.RPC("destroyThis", PhotonTargets.MasterClient);
                }
            }
            yield return null;
        }
    }

    [PunRPC]
    void destroyThis()
    {
        PhotonNetwork.Destroy(gameObject);
    }

    [PunRPC]
    void dismember(string name)
    {
        Destroy(GameObject.Find(name));
    }
    void OnPhotonPlayerConnected()
    {
        if (PhotonNetwork.isMasterClient)
        {
            playerTargets.AddRange(GameObject.FindGameObjectsWithTag("Player"));
        }
    }

    void OnPhotonPlayerDisconnected()
    {

        if (PhotonNetwork.isMasterClient)
        {
            playerTargets = new List<GameObject>();
            playerTargets.AddRange(GameObject.FindGameObjectsWithTag("Player"));

            if (transform.tag == "Enemy")
            {
                currentTarget = PhotonView.Find(int.Parse((PhotonNetwork.masterClient.ID + "001").ToString())).gameObject;
            }
            else
            {
                currentTarget = transform.gameObject;
                
            }

            photonView.TransferOwnership(PhotonNetwork.masterClient);
            
        }
      //  PhotonNetwork.DestroyPlayerObjects();
    }
}
