﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JanitorScript : MonoBehaviour {
	public static JanitorScript CleanEnvironment;

	public int GibletsCTR;
	public int BloodSpriteCTR;

	private PlayerProjectile projectileReference;

	// Use this for initialization
	void Awake(){
		CleanEnvironment=this;
	}
	// Update is called once per frame
	void Update () {
		if(BloodSpriteCTR>32){
			Destroy(GameObject.FindGameObjectWithTag("BloodSplatter"));
			BloodSpriteCTR--;
		}
	}
}
