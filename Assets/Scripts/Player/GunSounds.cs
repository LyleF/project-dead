﻿using UnityEngine;
using System.Collections;

public class GunSounds : MonoBehaviour {
    public AudioClip playerShoot;
    AudioSource playerAudio;
	// Use this for initialization
	void Start () {
        playerAudio = GetComponent<AudioSource>();
        playerAudio.clip = playerShoot;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
