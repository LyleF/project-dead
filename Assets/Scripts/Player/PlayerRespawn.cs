﻿using UnityEngine;
using System.Collections;

public class PlayerRespawn : MonoBehaviour {
	public static PlayerRespawn playerRespawn;

	private Vector3 Direction;
	// Use this for initialization
	void Start () {
		playerRespawn=this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void respawn(){
		//		Vector3 tempPos;
		//		GameObject[] playersAroundSpawn=GameObject.FindGameObjectsWithTag("Player");
		//
		int randomSpawn = Random.Range(1,7);
		//		Debug.Log(randomSpawn);
		//		tempPos=GameObject.Find("SpawnPoint ("+randomSpawn+")").transform.position;
		//
		//		foreach(GameObject player in playersAroundSpawn){
		//			if(Vector3.Distance(tempPos,player.transform.position)<3){
		//				randomSpawn = Random.Range(1,7);
		//				tempPos=GameObject.Find("SpawnPoint ("+randomSpawn+")").transform.position;
		//			}else
		//				break;
		//		}		
		Direction.x=0;
		Direction.y=0;
		Direction.z=0;
		Direction = transform.TransformDirection(Direction);
		transform.position=GameObject.Find("SpawnPoint ("+randomSpawn+")").transform.position;
		PlayerHP.playerHP.resetHP(100); //add max hp for selected perk
		//hp=100;
	}
}
