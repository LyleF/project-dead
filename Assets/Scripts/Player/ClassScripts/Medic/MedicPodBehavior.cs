﻿using UnityEngine;
using System.Collections;

public class MedicPodBehavior : Photon.MonoBehaviour {
    public float lifeTime;

    public float networkedTime;
    public float networkedLightIntensity;
    public bool allowHealing;

    public bool networkedHealing;
    // Use this for initialization
    void Start() {

    }

    void Update() {
        if (PhotonNetwork.isMasterClient)
        {
            //photonView.RPC("incrementTime", PhotonTargets.All);
            if (lifeTime < 3f && GetComponent<Rigidbody>().velocity.magnitude == 0)
            {
                lifeTime += Time.deltaTime;
                allowHealing = true;

                photonView.RPC("emit", PhotonTargets.All);
            }
            if (lifeTime >= 3)
            {
                if (transform.Find("Point light").GetComponent<Light>().intensity > 0f)
                {
                    transform.Find("Point light").GetComponent<Light>().intensity -= 1 / 60f;
                }
                else
                {
                    allowHealing = false;
                    photonView.RPC("destroyMe", PhotonTargets.All);
                }
            }
        }
        else
        {
            if (allowHealing)
            {
                transform.Find("Point light").GetComponent<Light>().intensity -= networkedLightIntensity;
               // GetComponent<ParticleSystem>().enableEmission = true;
            }
        }
    }

    [PunRPC]
    void destroyMe()
    {
        GetComponent<ParticleSystem>().enableEmission = false;
        Destroy(gameObject, 2f);
    }

    [PunRPC]
    void incrementTime()
    {
        if (lifeTime < 3f)
        {
            lifeTime += Time.deltaTime;
        }
    }
    [PunRPC]
    void emit()
    {
        GetComponent<ParticleSystem>().enableEmission = true;
    }
   

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
         //   stream.SendNext(lifeTime);
          //  stream.SendNext(transform.FindChild("Point light").GetComponent<Light>().intensity);
            stream.SendNext(allowHealing);
        }else
            if (stream.isReading)
        {
          //  networkedTime = (float)stream.ReceiveNext();
           // networkedLightIntensity = (float)stream.ReceiveNext();
            networkedHealing = (bool)stream.ReceiveNext();
        }
    }
}

