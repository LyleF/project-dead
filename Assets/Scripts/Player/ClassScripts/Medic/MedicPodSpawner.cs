﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MedicPodSpawner : Photon.MonoBehaviour {

    private GameObject myImage;
	// Use this for initialization
	void Start () {
        if (photonView.isMine)
        {
            GameObject.Find("Sharp_Skill").SetActive(false);
            GameObject.Find("CQB_Skill").SetActive(false);

            myImage = GameObject.Find("Medic_Skill");
        }
    }
	
	// Update is called once per frame
	void Update () {
	   
	}

    public void spawnPod()
    {

        if(myImage == null)
        {
            myImage = GameObject.Find("Medic_Skill");
        }

        float fb = Input.GetAxis("Vertical")*GetComponent<MovementScript>().playerSpeed;
        float lr = Input.GetAxis("Horizontal") * GetComponent<MovementScript>().playerSpeed;

        if (Input.GetKeyDown(KeyCode.V) && myImage.GetComponent<Image>().fillAmount == 1)
        {
            GameObject myPod = PhotonNetwork.Instantiate("MedicBomb", transform.Find("PlayerCameraFPS").position, Quaternion.identity, 0, null) as GameObject;

            if (fb == 0)
                myPod.GetComponent<Rigidbody>().AddForce(transform.Find("PlayerCameraFPS").forward * 500);
            else
                myPod.GetComponent<Rigidbody>().AddForce(transform.Find("PlayerCameraFPS").forward * 1000);

            myImage.GetComponent<Image>().color = new Color(255, 0, 0);
            myImage.GetComponent<Image>().fillAmount = 0.125f;
        }
        
        if(myImage.GetComponent<Image>().fillAmount < 1)
        {
            myImage.GetComponent<Image>().fillAmount += 0.05f * Time.deltaTime;
        }

        if (myImage.GetComponent<Image>().fillAmount == 1)
        {
            myImage.GetComponent<Image>().color = new Color(255, 255, 255);
        }
    }
}
