﻿using UnityEngine;
using System.Collections;

public class Drone : Photon.MonoBehaviour {
	private GameObject restingPosition;
	private GameObject enemyTarget;
	// Use this for initialization
	void Start () {
        if (photonView.isMine)
        {
            restingPosition = GameObject.Find("YourPlayer").transform.Find("DroneRestingPosition").gameObject;
        }
        else
            restingPosition = GameObject.Find("NetworkPlayer").transform.Find("DroneRestingPosition").gameObject;
	}

	// Update is called once per frame
	void Update () {
		MoveDrone();
	}

	void MoveDrone(){
		Quaternion targetRotation=Quaternion.LookRotation(restingPosition.transform.position-transform.position);
		transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation,5*Time.deltaTime);

		transform.position=Vector3.MoveTowards(transform.position, restingPosition.transform.position,30*Time.deltaTime); 
	}
}
