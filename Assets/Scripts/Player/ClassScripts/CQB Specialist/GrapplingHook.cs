﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
public class GrapplingHook : Photon.MonoBehaviour {

	public Transform playerCameraFPS;
	public Transform playerCameraTPS;

	private GameObject persistentValues;

	private RaycastHit hit;
	private Rigidbody rigidBody;

	private LineRenderer lineRenderer;

	private float renderCount;
	private float renderDist;

	private Transform origin;

	private Vector3 destination;

	private float drawSpeed = 3.5f;

	private float momentum;
	private float speed;
	private float step;

    private Vector3 networkDestination;
    private Vector3 networkOrigin;
    private float networkRenderCount;
    private bool networkAttached;

	public bool attached;

    internal bool fireHook;
    internal bool allowedToFire;

    private GameObject myImage;

    private List<GameObject> crossHairParts;

	void Awake(){
		persistentValues = GameObject.Find("PersistentValues");

        if (!photonView.isMine)
        {
            StartCoroutine("Sync");
        }
        else
        {

            if (persistentValues.GetComponent<PersistentValues>().getCQBClass() || persistentValues.GetComponent<PersistentValues>().getBM())
                origin = transform.Find("PlayerCameraFPS").Find("Grapple").transform;

            GameObject.Find("Sharp_Skill").SetActive(false);
            GameObject.Find("Medic_Skill").SetActive(false);
            crossHairParts = new List<GameObject>();

            myImage = GameObject.Find("CQB_Skill");
            crossHairParts.AddRange(GameObject.FindGameObjectsWithTag("Crosshair"));
        }
            lineRenderer = GetComponent<LineRenderer>();
        
      //  myImage.GetComponent<Image>().fillMethod = Image.FillMethod.Vertical;
	}
	void Start () {

		rigidBody=GetComponent<Rigidbody>();
		speed=100;

        
	}

    public void hookShot(){

        if (crossHairParts == null)
        {
            crossHairParts = new List<GameObject>();
            crossHairParts.AddRange(GameObject.FindGameObjectsWithTag("Crosshair"));

            lineRenderer = GetComponent<LineRenderer>();
            myImage = GameObject.Find("CQB_Skill");
            rigidBody = GetComponent<Rigidbody>();
            speed = 100;
        }
        if (Physics.Raycast(playerCameraFPS.position, playerCameraFPS.forward, out hit))
        {
            if(hit.collider.tag!="Player" && hit.collider.tag!="Survivor" && hit.transform.tag!="Enemy"&& hit.collider.tag != "Blockade") 
            if (hit.distance <= 40)
            {
                allowedToFire = true;

                foreach (GameObject cross in crossHairParts)
                {
                    cross.GetComponent<Image>().color = Color.red;
                }
            }
            else
            {
                allowedToFire = false;
                foreach (GameObject cross in crossHairParts)
                {
                    cross.GetComponent<Image>().color = Color.white;
                }
            }
            if (fireHook) {
			if (playerCameraFPS.gameObject.activeInHierarchy) {
                    if (allowedToFire)//set max distance
                    {
                        GetComponent<MovementScript>().Direction.y = 0;
                        attached = true;
                    }
                }
			} else if (playerCameraTPS.gameObject.activeInHierarchy) {
				if (Physics.Raycast (playerCameraTPS.position, playerCameraTPS.position * 10, out hit)) { //set max distance
					attached = true;
				}
			}
		}
		if(!fireHook){
			attached=false;
		}

		if(attached){
			momentum+=Time.deltaTime*speed;

            if (destination == Vector3.zero)
            {
                destination = hit.point;
                Debug.Log("empty supposedly");
            }
            
            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);


            renderDist = Vector3.Distance(origin.position,destination);

			if(renderCount<renderDist){
				renderCount+=.1f/drawSpeed;

				float x = Mathf.Lerp(0,renderDist,renderCount);

				Vector3 pointA = origin.position;
				Vector3 pointB = destination;

				Vector3 PointDrawn = x *Vector3.Normalize(pointB-pointA)+pointA;

				lineRenderer.SetPosition(0,origin.position);
				lineRenderer.SetWidth(.45f,.45f);
				lineRenderer.SetPosition(1,PointDrawn);
			}

            if(Vector3.Distance(transform.position,destination) < 5f)
            {
                attached = false;
                fireHook = false;
            }
            myImage.GetComponent<Image>().color = new Color(255, 0, 0);
            myImage.GetComponent<Image>().fillAmount = 0.125f;
        }

		if(!attached){
			renderCount=0;
			renderDist=0;

			lineRenderer.SetPosition(0,origin.position);
			lineRenderer.SetPosition(1,origin.position);

            if (momentum >= 0)
            {
                momentum += Time.deltaTime * (speed/2);
            }

            destination = Vector3.zero;
		}
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    { //player is observing things coming from this method
        if (stream.isWriting)
        { //things the player is sending
            stream.SendNext(destination);
            stream.SendNext(origin.position);
            stream.SendNext(renderCount);
            stream.SendNext(attached);
        }
        else
            if (stream.isReading)
            {
                networkDestination = (Vector3)stream.ReceiveNext();
                networkOrigin = (Vector3)stream.ReceiveNext();
                networkRenderCount = (float)stream.ReceiveNext();
                networkAttached = (bool)stream.ReceiveNext();
            }
    }
    IEnumerator Sync()
    {
        while (true) {
            if (networkAttached)
            {
                lineRenderer.enabled = true;
                Vector3 pointA = networkOrigin;
                Vector3 pointB = networkDestination;
                float renderDist = Vector3.Distance(pointA, pointB);
                float x = Mathf.Lerp(0, renderDist, networkRenderCount);

                Vector3 PointDrawn = x * Vector3.Normalize(pointB - pointA) + pointA;
                lineRenderer.SetPosition(0, pointA);
                lineRenderer.SetWidth(.45f, .45f);
                lineRenderer.SetPosition(1, PointDrawn);
            }else
            if(!networkAttached)
            {
                if (lineRenderer != null)
                {
                    lineRenderer.SetPosition(0, new Vector3(0, 0, 0));
                    lineRenderer.SetPosition(1, new Vector3(0, 0, 0));
                }
            }
            yield return null;
        }
    }
    private void checkParent(){
		if (transform.Find ("PlayerCameraFPS") == null) {
			origin.SetParent (GameObject.Find("PlayerCameraTPS").transform);
		} else 
			if (transform.Find ("PlayerCameraTPS") == null) {
				origin.SetParent (GameObject.Find("PlayerCameraFPS").transform);
		}
	}
}
