﻿using UnityEngine;
using System.Collections;

public class GrapplingHookExtension : MonoBehaviour {
	private float targetSize = 5;
	private float currentSize;

	private GameObject rope;

	void Awake(){
	}
	void Start () {
		currentSize=GetComponent<Collider>().bounds.size.z;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Alpha9)&&transform.localScale.z<3f&&transform.localScale.z>=0f){
			targetSize+=Time.deltaTime;
			resize();
		}
		if(Input.GetKeyDown(KeyCode.Alpha0)&&transform.localScale.z>3f){
			targetSize-=Time.deltaTime;
			resize();
		}

	}
	void resize(){
		Vector3 scale = transform.localScale;

		scale.z = targetSize * scale.z / currentSize;
			
		transform.localScale=scale;
	}
}
