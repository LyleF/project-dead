﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class KrissVectorScript : Primary {

    private NetworkManager netman;
	public static KrissVectorScript _kriss;

    private int damage;

	public bool isAuto = true;

    private bool CanFire;
    private bool isReloading;

	private GameObject projectileCreator;
    public GameObject _projectile;
    public GameObject muzzleSpawn;

    public AudioClip gunSound;
    private AudioSource gunAudio;

    private GameObject KrissImage;
    private GameObject GolImage;
    private GameObject L86Image;
    private GameObject BladeImage;
    private GameObject M240BImage;

    void Start () {
        
		_kriss=this;

        if (transform.name == "Kriss")
        {
            InitialMagCap = 30;
            recoilModifier = 0.1f;

            MagazineCapacity = InitialMagCap;
            AmmoSum = 300;

            rateOfFire = 0.05f;
        }

        if (transform.name == "L86LSW")
        {
            InitialMagCap = 100;
            recoilModifier = 0.475f;

            MagazineCapacity = InitialMagCap;
            AmmoSum = 1000;

            rateOfFire = 0.0875f;
        }

        if (transform.name == "M27IAR")
        {
            InitialMagCap = 45;
            recoilModifier = 0.3f;

            MagazineCapacity = InitialMagCap;
            AmmoSum = 1000;

            rateOfFire = 0.08f;
        }
        if (transform.name == "M240B")
        {
            InitialMagCap = 150;
            recoilModifier = 0.7f;

            MagazineCapacity = InitialMagCap;
            AmmoSum = 1000;

            rateOfFire = 0.09f;
        }
        if (transform.name == "SS Hand Gol Magnum")
        {
            InitialMagCap = 10;
            recoilModifier = 0.875f;

            MagazineCapacity = InitialMagCap;
            AmmoSum = 100;

            rateOfFire = 0.5f;
        }
		delayFireTime=69;
		//rateOfFire=0.05f;

        netman = FindObjectOfType<NetworkManager>();

	}
	void OnEnable(){
        //KrissImage = GameObject.Find("KRISS_AmmoCounter");
        //GolImage = GameObject.Find("GOL_AmmoCounter");
        //M240BImage = GameObject.Find("M240B_AmmoCounter");
        //BladeImage = GameObject.Find("BLADE_AmmoCounter");

        //if (transform.name.Contains("Kriss"))
        //{
        //    GolImage.SetActive(false);
        //    M240BImage.SetActive(false);
        //    BladeImage.SetActive(false);
        //}

        MagCap =GameObject.Find("MagazineCtrText");
		AmmoPool=GameObject.Find("TotalAmmoText");

		MagCap.GetComponent<Text>().text = MagazineCapacity.ToString();
		AmmoPool.GetComponent<Text>().text= AmmoSum.ToString();
	}

	void Update () {
        
        if (transform.name == "Kriss")
        {
            damage = 30;
            if (Input.GetKey(KeyCode.Mouse0) && !(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Full reload")))
            {
                GetComponent<PhotonView>().RPC("_Shoot", PhotonTargets.All);
                
                delayFireTime += Time.deltaTime * 1f;
            }

            if (!netman.GetComponent<NetworkManager>().repositionEveryone)
            {
                AmmoSum = 300;
                MagazineCapacity = 30;
            }
        }else
            if (transform.name == "L86LSW")
        {
            damage = 45;

            if (Input.GetKey(KeyCode.Mouse0) && !(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Full reload")))
            {
                GetComponent<PhotonView>().RPC("_Shoot", PhotonTargets.All);
        
                delayFireTime += Time.deltaTime * 1f;
            }

            if (!netman.GetComponent<NetworkManager>().repositionEveryone)
            {
                AmmoSum = 1000;
                MagazineCapacity = 100;
            }

        }
        else if (transform.name == "M27IAR")
        {
            damage = 45;

            if (Input.GetKey(KeyCode.Mouse0) && !(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Full reload")))
            {
                GetComponent<PhotonView>().RPC("_Shoot", PhotonTargets.All);

                delayFireTime += Time.deltaTime * 1f;
            }

            if (!netman.GetComponent<NetworkManager>().repositionEveryone)
            {
                AmmoSum = 1000;
                MagazineCapacity = 45;
            }

        }
        else if (transform.name == "M240B")
        {
            damage = 55;

            if (Input.GetKey(KeyCode.Mouse0) && !(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Full reload")))
            {
                GetComponent<PhotonView>().RPC("_Shoot", PhotonTargets.All);
                delayFireTime += Time.deltaTime * 1f;
            }

            if (!netman.GetComponent<NetworkManager>().repositionEveryone)
            {
                AmmoSum = 1000;
                MagazineCapacity = 150;
            }

        }

        else
         if (transform.name== "SS Hand Gol Magnum")
        {
            damage = 150;

            if (!(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Full reload")))
            {
                if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Shoot") && !CanFire && MagazineCapacity > 0)
                {
                    CanFire = true;
                    GetComponent<PhotonView>().RPC("_Shoot", PhotonTargets.All);
                }
                else
                    if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Shoot"))
                        CanFire = false;
            }

            if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Full reload") && !isReloading )
            {
                isReloading = true;

                if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
                    MagazineCapacity = InitialMagCap;
            }
            else
                isReloading = false;

            if (!netman.GetComponent<NetworkManager>().repositionEveryone)
            {
                AmmoSum = 100;
                MagazineCapacity = 10;
            }
        }

        MagCap.GetComponent<Text>().text = MagazineCapacity.ToString();
        AmmoPool.GetComponent<Text>().text = AmmoSum.ToString();
    }

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if(stream.isWriting){
			stream.SendNext(MagazineCapacity);
			stream.SendNext(delayFireTime);
            stream.SendNext(rateOfFire);
		}else
			if(stream.isReading){
				MagazineCapacity = (int)stream.ReceiveNext();
				delayFireTime = (float)stream.ReceiveNext();
                rateOfFire = (float)stream.ReceiveNext();
			}
	}

	[PunRPC]
	public void _Shoot(){
	//	Shoot(15000,10,0.05f,0.5f,false);	
		if(MagazineCapacity!=0){
			if(delayFireTime>=rateOfFire||transform.tag=="SniperRifle"){
         
                GameObject projectile = PhotonNetwork.Instantiate("Projectile", Barrel.transform.position, Quaternion.identity, 0,null);
                projectile.GetComponent<PlayerProjectile>().damage = damage;
                if (transform.tag != "SniperRifle")
                    projectile.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward * 15000);
                else
                    projectile.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward * 35000);
                GetComponentInParent<MovementScript>().playerRotX+=recoilModifier;
				GetComponentInParent<MovementScript>().playerRotY+=Random.Range(-recoilModifier/2,recoilModifier);

				delayFireTime=0;
				MagazineCapacity--;

                GetComponent<PhotonView>().RPC("playSound", PhotonTargets.All);
                int a = 1, b = 2, c = 0;

                c = a + b;
            }
	    }
	}
    [PunRPC]
    public void playSound()
    {
        gunAudio = GetComponent<AudioSource>();
     //   gunAudio.clip = gunSound;
        AudioSource.PlayClipAtPoint(gunSound,transform.position);
    }

}
	