
using UnityEngine;
using System.Collections;

public interface IFireArms{
	void Shoot(float force,float damage,float rateOfFire,float RecoilRate, bool isSemiAuto);
//	void SemiAutoFire(float force,float damage,float rateOfFire,float RecoilRate);
//	void FullAutoFire(float force,float damage,float rateOfFire,float RecoilRate);
	void Reload();
	void Aim();
}

// public void CreateProjectile(Vector3 Pos, Vector3 Rot, float force)