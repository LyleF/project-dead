﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Weapon : MonoBehaviour,IFireArms {
	
	public GameObject Barrel;
	public GameObject PlCam;

	internal GameObject MagCap;
	internal GameObject AmmoPool;
	private GameObject projectilePool;

	internal AudioSource weaponFireClip;
	internal bool isEmpty;
	internal float damage;
	internal float rateOfFire;
	internal float delayFireTime;
	internal float reloadTime;
	internal float recoilModifier;


	public int MagazineCapacity;
	public int AmmoSum;

	internal int InitialMagCap;

	internal int DeductedFromSum;

	internal Vector3 initialWeaponPos;

    internal int networkAmmoSum;
    internal int networkMagCap;
    internal float networkDelayfire;
    internal float networkRateOfFire;
	void Awake(){
		projectilePool = GameObject.Find("ProjectilePooler");
	}
	void Start(){
		delayFireTime=69;

		InitialMagCap=MagazineCapacity;
	}
	void OnEnable(){
		initialWeaponPos=transform.localPosition;

	}

	public void Shoot(float force,float damage,float rateOfFire,float RecoilRate, bool isSemiAuto){
		if(isSemiAuto&&MagazineCapacity!=0){
			if(delayFireTime>rateOfFire){
				if(Input.GetKeyDown(KeyCode.Mouse0)){
					GameObject playerProjectile = projectilePool.GetComponent<ProjectileCreation>().getBullets();
					if(playerProjectile==null) 
						return;
					playerProjectile.transform.position = Barrel.transform.position;
					playerProjectile.transform.rotation=Barrel.transform.rotation;
					playerProjectile.SetActive(true);
					playerProjectile.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward*force);

					GetComponentInParent<MovementScript>().playerRotX+=RecoilRate;
					GetComponentInParent<MovementScript>().playerRotY+=Random.Range(-RecoilRate/2,RecoilRate);
			
					delayFireTime=0;
					MagazineCapacity--;

				}
			}
		}else
			if(!isSemiAuto&&MagazineCapacity!=0){
				if(delayFireTime>rateOfFire){
					projectilePool.GetComponent<ProjectileCreation>().CreateProjectile(Barrel.transform,Barrel.transform.rotation,force);
						delayFireTime=0;

						GetComponentInParent<MovementScript>().playerRotX+=RecoilRate;
						GetComponentInParent<MovementScript>().playerRotY+=Random.Range(-RecoilRate/2,RecoilRate);

						MagazineCapacity--;
		
				}
			}
		delayFireTime+=Time.deltaTime;
		}

//	public void SemiAutoFire(float force,float damage,float rateOfFire,float RecoilRate){
//		if(MagazineCapacity!=0){
//			if(delayFireTime>rateOfFire){
//					GameObject playerProjectile = ProjectileCreation.CreateProjectile.getBullets();
//					if(playerProjectile==null) 
//						return;
//					playerProjectile.transform.position = Barrel.transform.position;
//					playerProjectile.transform.rotation=Barrel.transform.rotation;
//					playerProjectile.SetActive(true);
//					playerProjectile.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward*force);
//
//					GetComponentInParent<MovementScript>().playerRotX+=RecoilRate;
//					GetComponentInParent<MovementScript>().playerRotY+=Random.Range(-RecoilRate/2,RecoilRate);
//
//					delayFireTime=0;
//					MagazineCapacity--;
//
//			}
//		}
//		delayFireTime+=Time.deltaTime;
//	}
//	public void FullAutoFire(float force,float damage,float rateOfFire,float RecoilRate){
//		if(MagazineCapacity!=0){
//			if(delayFireTime>rateOfFire){
//				GameObject playerProjectile = ProjectileCreation.CreateProjectile.getBullets();
//				if(playerProjectile==null) 
//					return;
//				playerProjectile.transform.position = Barrel.transform.position;
//				playerProjectile.transform.rotation=Barrel.transform.rotation;
//				playerProjectile.SetActive(true);
//				playerProjectile.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward*force);
//
//				GetComponentInParent<MovementScript>().playerRotX+=RecoilRate;
//				GetComponentInParent<MovementScript>().playerRotY+=Random.Range(-RecoilRate/2,RecoilRate);
//
//				delayFireTime=0;
//				MagazineCapacity--;
//
//			}
//		}
//		delayFireTime+=Time.deltaTime;
//	}
	public void Reload(){
		if(AmmoSum>0){
			if(MagazineCapacity<InitialMagCap){
				DeductedFromSum=InitialMagCap-MagazineCapacity;
				AmmoSum-=DeductedFromSum;

				MagazineCapacity=InitialMagCap;
		}
	}
}
	public void Aim(){

	}
}
