﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GolMagnum : Primary {
	public static GolMagnum _GOL;
	void Start () {
		_GOL=this;

		InitialMagCap=5;

		MagazineCapacity=InitialMagCap;

		AmmoSum=30;
	}

	void OnEnable(){
		MagCap=GameObject.Find("MagazineCtrText");
		AmmoPool=GameObject.Find("TotalAmmoText");

		MagCap.GetComponent<Text>().text = MagazineCapacity.ToString();
		AmmoPool.GetComponent<Text>().text= AmmoSum.ToString();
	}

	void Update () {
		
		if(Input.GetKeyDown(KeyCode.R))
			Reload();

		MagCap.GetComponent<Text>().text = MagazineCapacity.ToString();
		AmmoPool.GetComponent<Text>().text= AmmoSum.ToString();
	}
	public void _Shoot(){
		//SemiAutoFire(50000,1,1,5f);
	}
}
