﻿using UnityEngine;
using System.Collections;

public class DroppedWeaponScript : MonoBehaviour {

	private string WeaponName;

	private MonoBehaviour PickedUpWeaponInheritance;

	public bool CollidedWithPlayer;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col){

		if(col.gameObject.tag=="Player"){
			CollidedWithPlayer=true;

			Transform[] ParentTrans=GetComponentsInParent<Transform>();
			foreach(Transform childTrans in ParentTrans){
				if(childTrans.gameObject.transform.parent==null){
					WeaponName=childTrans.name;

					if(PickedUpWeaponInheritance is Primary)
						Debug.Log("use this instead");
				}
			}
		}else
			CollidedWithPlayer=false;
	}
	public string getWeaponName(){
		return WeaponName;
	}
}

