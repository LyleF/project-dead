﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponBobbing : Photon.MonoBehaviour {
    public static WeaponBobbing wepBobRef;

    private GameObject CrossHair;
    public GameObject muzzleSpawn;
    public Camera playerCam;

    public bool isAiming;

	private float MouseX;
	private float MouseY;

    private float defFov;

    public Transform AimPos;

    private Vector3 hipPos;

    private Vector3 muzzleHip;
    private Vector3 muzzleAim;

	private Quaternion rotationSpeed;

    public bool isSniper;

    private GameObject sniperImage;

    private Transform GolLeftHand;
    private Transform GolRightHand;
            
    private Transform GolMag;
    private Transform GolBolt;
    private Transform GolBody;

	// Use this for initialization
	void Start () {
        if (photonView.isMine)
        {
            GolLeftHand = transform.Find("polySurface35");
            GolRightHand = transform.Find("polySurface25");
            GolMag = transform.Find("polySurface62");
            GolBody = transform.Find("polySurface68");

            wepBobRef = this;

            hipPos = transform.localPosition;
            defFov = playerCam.fieldOfView;

            sniperImage = GameObject.Find("Scope");

            CrossHair = GameObject.Find("CrossHair");

            if (transform.tag == "SniperRifle")
                isSniper = true;

            sniperImage.GetComponent<Image>().enabled = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (photonView.isMine)
        {
            Bobbing();


            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                if (isAiming || MovementScript._MovementScript.isSprinting)
                {
                    isAiming = false;
                }
                else
                    if (!isAiming)
                {
                    isAiming = true;
                }
            }
            if (isAiming && MovementScript._MovementScript.isSprinting)
            {
                isAiming = false;
            }

            Aim();
        }
	}

    void Bobbing()
    {

        MouseX = Input.GetAxis("Mouse X");
        MouseY = Input.GetAxis("Mouse Y");

        if (MovementScript._MovementScript != null)
        {
            if (!MovementScript._MovementScript.isSprinting)
            {
                if(transform.name!="M240B")
                 rotationSpeed = Quaternion.Euler(-MouseY, -MouseX + 90, 0);
                else
                {
                    rotationSpeed = Quaternion.Euler(-MouseY, -MouseX - 90, 0);
                }
            }
            else
                if (MovementScript._MovementScript.isSprinting)
            {
                if(transform.name!="M27IAR" && transform.name!="M240B")
                rotationSpeed = Quaternion.Euler(-MouseY - 20, -MouseX + 90, 0);
                else
                {
                    if(transform.name=="M27IAR")
                    rotationSpeed = Quaternion.Euler(-MouseY - 10, -MouseX + 90, 0);
                    if(transform.name=="M240B")
                    rotationSpeed = Quaternion.Euler(-MouseY + 10, -MouseX - 90, 0);
                }
            }
        }
        transform.localRotation = Quaternion.Slerp(transform.localRotation, rotationSpeed, 3 * Time.deltaTime);
    }

    void Aim()
    {
        
        if(transform.name == "Kriss")
        {
            if (isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, AimPos.transform.localPosition, 100 * Time.deltaTime);
                CrossHair.SetActive(false);
            }
            else
            if(!isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, hipPos, 100 * Time.deltaTime);
                CrossHair.SetActive(true);
            }
        }

        if(transform.name == "Mossberg")
        {
            if (isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, AimPos.transform.localPosition, 40 * Time.deltaTime);
                CrossHair.SetActive(false);
            }
            else
          if (!isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, hipPos, 40 * Time.deltaTime);

                CrossHair.SetActive(true);
            }
        }
        if(transform.name == "L86LSW")
        {
            if (isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, AimPos.transform.localPosition, 100 * Time.deltaTime);
                   CrossHair.SetActive(false);
            }
            else
             if (!isAiming)
                {
                    transform.localPosition = Vector3.Slerp(transform.localPosition, hipPos, 100 * Time.deltaTime);

                      CrossHair.SetActive(true);
                }
        }
        if (transform.name == "M27IAR")
        {
            if (isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition,new Vector3(-0.847f, -0.24f, -0.474f), 100 * Time.deltaTime);
                //transform.localPosition = Vector3.Slerp(transform.localPosition, AimPos.transform.localp, 100 * Time.deltaTime);
                //   CrossHair.SetActive(false);
            }
            else
                if (!isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, new Vector3(-0.648f, -0.313f, -0.04f), 100 * Time.deltaTime);

                //    CrossHair.SetActive(true);
            }
        }
        if (transform.name == "M240B")
        {
            if (isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, new Vector3(-0.462f, -0.537f, 0.665f), 100 * Time.deltaTime);
                
                //transform.localPosition = Vector3.Slerp(transform.localPosition, AimPos.transform.localp, 100 * Time.deltaTime);
                //   CrossHair.SetActive(false);
            }
            else
                if (!isAiming)
            {
                transform.localPosition = Vector3.Slerp(transform.localPosition, new Vector3(-0.145f, -0.6f, 1.072f), 100 * Time.deltaTime);
                //    CrossHair.SetActive(true);
            }
        }
        if (transform.tag == "SniperRifle")
        {
            if(sniperImage == null)
            {
                GolLeftHand = transform.Find("polySurface35");
                GolRightHand = transform.Find("polySurface25");
                GolMag = transform.Find("polySurface62");
                GolBody = transform.Find("polySurface68");

                wepBobRef = this;

                hipPos = transform.localPosition;
                defFov = playerCam.fieldOfView;

                sniperImage = GameObject.Find("Scope");

                CrossHair = GameObject.Find("CrossHair");

                if (transform.tag == "SniperRifle")
                    isSniper = true;

                sniperImage.GetComponent<Image>().enabled = false;
            }
            if (isAiming)
            {
                sniperImage.GetComponent<Image>().enabled = true;
                CrossHair.SetActive(false);

                GolBody.gameObject.SetActive(false);
                GolMag.gameObject.SetActive(false); 
                GolLeftHand.gameObject.SetActive(false);
                GolRightHand.gameObject.SetActive(false);
            }
            else
            {
                sniperImage.GetComponent<Image>().enabled = false;
                CrossHair.SetActive(true);

                GolBody.gameObject.SetActive(true);
                GolMag.gameObject.SetActive(true);
                GolLeftHand.gameObject.SetActive(true);
                GolRightHand.gameObject.SetActive(true);
            }
            
        }
    }
}
