﻿using UnityEngine;
using System.Collections;

public class WeaponPickUp : MonoBehaviour {
	[SerializeField]
	private Transform FPSCam;

	private Transform Primary;
	private Transform Secondary;
	private Transform Melee;
	private Transform Equipment;

	private Transform CurrentWeapon;
	private Transform PreviousWewapon;

	private Transform PickedUpWeapon;

	private MonoBehaviour PickedUpWeaponInheritance;

	private string WeaponName;
	private bool isPickUp;

	void Awake(){
		foreach(Transform weapon in FPSCam){
			if(weapon.gameObject.GetComponent<MonoBehaviour>() is Primary){
				Primary = weapon;
			}
			if(weapon.gameObject.GetComponent<MonoBehaviour>() is Secondary){
				Secondary = weapon;
			}
		}
	}
	void Start(){
		
	}
	void Update () {

		WeaponSwapping();
	}
    void LateUpdate()
    {

    }


	//Pick Up by Raycast
//	void PickUp(){
//		RaycastHit hit;
//
//			if(Physics.Raycast(transform.position,transform.forward,out hit,2f)){
//				Transform[] ParentTrans=hit.collider.GetComponentsInParent<Transform>();
//					foreach(Transform childTrans in ParentTrans){
//						if(childTrans.gameObject.transform.parent==null){
//							if(childTrans.tag=="PickUpWep"){
//						
//								isPickUp=true;
//							
//								if(Input.GetKeyDown(KeyCode.E)){	
//								Transform PickedUpWeapon=transform.FindChild(childTrans.name);
//								PickedUpWeaponInheritance=PickedUpWeapon.GetComponent<MonoBehaviour>();
//								
//								if(PickedUpWeaponInheritance is Primary)
//									Primary=PickedUpWeapon;
//								if(PickedUpWeaponInheritance is Secondary)
//									Secondary=PickedUpWeapon;
//						}
//					}
//				}
//			}
//		}
//	}


	void WeaponSwapping(){
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			if(Primary!=null){
				Primary.gameObject.SetActive(true);

				if(Secondary!=null)
					Secondary.gameObject.SetActive(false);
//				Melee.gameObject.SetActive(false);
//				Equipment.gameObject.SetActive(false);
			}
		}

		if(Input.GetKeyDown(KeyCode.Alpha2)){
			if(Secondary!=null){
				Secondary.gameObject.SetActive(true);

				if(Primary!=null)
					Primary.gameObject.SetActive(false);
//				Melee.gameObject.SetActive(false);
//				Equipment.gameObject.SetActive(false);
			}
		}
	}

	public bool isPrimaryActive(){
		if(Primary!=null)
			return true;
		else
			return false;
	}
	public string getWeaponName(){
		return Primary.name;
	}
	public void setWeaponName(string wepName){
		Primary.name = wepName;
	}

	void OnCollisionEnter(Collision col){

		if(col.gameObject.tag=="PickUpWep"){
		Transform[] ParentTrans=col.gameObject.GetComponentsInParent<Transform>();
		foreach(Transform childTrans in ParentTrans){
			if(childTrans.gameObject.transform.parent==null){
					Debug.Log("UGH");
					isPickUp=true;
					
					if(Input.GetKeyDown(KeyCode.E)){	
						PickedUpWeapon=GameObject.Find("PlayerCamera").transform.Find(childTrans.name);
					
						PickedUpWeaponInheritance=PickedUpWeapon.GetComponent<MonoBehaviour>();

						if(PickedUpWeaponInheritance is Primary)
							Primary=PickedUpWeapon;
						if(PickedUpWeaponInheritance is Secondary)
							Secondary=PickedUpWeapon;

						Destroy(childTrans.gameObject);
				}
			}
		}
	}else
			isPickUp=false;
	}

}
