﻿	using UnityEngine;
using System.Collections;

public class ProjectileCleanUp : MonoBehaviour {

	// Use this for initialization
	void OnEnable(){
		Invoke("Destroy",3f);
	}
	void Destroy(){
		gameObject.SetActive(false);
	}
	void OnDisable(){
		CancelInvoke();
	}
}
