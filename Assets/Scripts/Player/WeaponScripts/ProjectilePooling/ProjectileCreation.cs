﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ProjectileCreation : Photon.MonoBehaviour {
	public static ProjectileCreation CreateProjectiles;

	public GameObject ProjectileRef;

	internal int bulletCount=50;

	internal List<GameObject> pooledProjectiles;

	private Vector3 pos;
	private Quaternion rot;
	// Use this for initialization	
	void Awake(){
		CreateProjectiles = this;
	}
	void Start () {
		pooledProjectiles = new List<GameObject>();
	}
	void Update(){
	}
	public void InitializeProjectiles(){
		for(int i=0;i<bulletCount;i++){
			//GameObject obj = PhotonNetwork.Instantiate("Projectile",transform.position,Quaternion.identity,0) as GameObject;
			GameObject obj = Instantiate(ProjectileRef) as GameObject;
			obj.SetActive(false);
			pooledProjectiles.Add(obj);
		}	
	}
//	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
//		if(stream.isWriting){
//			
//		}else
//			if(stream.isReading){
//			
//			}
//	}
	public GameObject getBullets(){
		for(int i=0;i<pooledProjectiles.Count;i++){
            if(pooledProjectiles[i] == null)
            {
                GameObject obj = Instantiate(ProjectileRef) as GameObject;
                obj.SetActive(false);
                pooledProjectiles[i] = obj;
                return pooledProjectiles[i];
            }
			if(!pooledProjectiles[i].activeInHierarchy)
				return pooledProjectiles[i];
		}

      //  GameObject obj = Instantiate(ProjectileRef) as GameObject;
       

		return null;
	}
    //[PunRPC]
	public void CreateProjectile(Transform pos, Quaternion  rot, float force){
		GameObject playerProjectile = getBullets();
		if(playerProjectile==null) 
			return;
		playerProjectile.transform.position = pos.position;
		playerProjectile.transform.rotation= rot;
		playerProjectile.SetActive(true);
		playerProjectile.GetComponent<Rigidbody>().AddForce(pos.forward*force);

	}
}
