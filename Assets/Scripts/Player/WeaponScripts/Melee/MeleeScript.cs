﻿using UnityEngine;
using System.Collections;

public class MeleeScript : MonoBehaviour {
    public GameObject parentAnimation;

    private Animator anims;

    private int Attack1State;
    private int Attack2State;
    private int Attack3State;

    private bool bladeCollided;
	// Use this for initialization
	void Start () {
        anims = parentAnimation.GetComponent<Animator>();

        Attack1State = Animator.StringToHash("Base.Attack1");
        Attack2State = Animator.StringToHash("Base.Attack2");
        Attack3State = Animator.StringToHash("Base.Attack3");
	}
	
	// Update is called once per frame
	void Update () {
      //  checkAttack();
	}

    void checkAttack(GameObject objectHit)
    {
        if (anims.GetCurrentAnimatorStateInfo(0).IsName("Attack1"))
        {
            if (bladeCollided)
            {
                Debug.Log("First Attack");
                bladeCollided = false;
            }
        }
        if (anims.GetCurrentAnimatorStateInfo(0).IsName("Attack2"))
        {
            if (bladeCollided)
            {
                Debug.Log("Second Attack");
                bladeCollided = false;
            }
        }
        if (anims.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
        {
            if (bladeCollided)
            {
                Debug.Log("Third Attack");
                bladeCollided = false;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer != LayerMask.NameToLayer("Weapons") && col.tag!="Player")
        {
            if (col.tag == "Enemy")
            {
                checkAttack(col.gameObject);
                bladeCollided = true;
            }
            Debug.Log("OBJECT THAT YOU HIT:" + col.gameObject);
        }
    }
}
