﻿using UnityEngine;
using System.Collections;

public class playerSkill : Photon.MonoBehaviour {

	private GameObject persistentValues;
	void Awake(){
        if(photonView.isMine)
		persistentValues = GameObject.Find("PersistentValues");
	}
	void Start () {
	}

	void Update () {
        if (photonView.isMine)
        {
            if (persistentValues.GetComponent<PersistentValues>().getCQBClass() || persistentValues.GetComponent<PersistentValues>().getBM())
            {
                GetComponent<GrapplingHook>().hookShot();
            }
            if (persistentValues.GetComponent<PersistentValues>().getSharp())
            {
                GetComponent<DroneSpawner>().spawnDrone();
            }
            if (persistentValues.GetComponent<PersistentValues>().getMedic())
            {
                GetComponent<MedicPodSpawner>().spawnPod();
            }
        }
	}
}
