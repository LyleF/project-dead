﻿using UnityEngine;
using System.Collections;

public class Movement1_1 : Photon.MonoBehaviour {
	[SerializeField]
	private Transform CameraTransform;
	[SerializeField]
	private GameObject Projectile;
	[SerializeField]
	private GameObject Barrel;

	private CharacterController playerCont;
	private Vector3 playerDirection;

	public float playerRotY;
	public float playerRotX;

	private float distanceToGround;

	private float jumpTimer=0.2f;

    private Vector3 OriginalPosition;
	// Use this for initialization
	void Start () {
        
        OriginalPosition = transform.position;

        CameraTransform = transform;
		playerCont=GetComponent<CharacterController>();

		playerRotX=CameraTransform.transform.localEulerAngles.x;
		playerRotY=transform.localEulerAngles.y;

		playerDirection=Vector3.zero;
		distanceToGround=GetComponent<Collider>().bounds.extents.y;
	}
	
	// Update is called once per frame
	void Update () {
		Movement();
	}
		
	void Movement(){

		playerRotX+=Input.GetAxis("Mouse Y")*1f;

		playerRotX=Mathf.Clamp(playerRotX,-50,50);

		playerRotY+=Input.GetAxis("Mouse X")*1f;

		transform.localEulerAngles=new Vector3(transform.localEulerAngles.x,playerRotY,transform.localEulerAngles.z);

		CameraTransform.localEulerAngles = new Vector3(-playerRotX,CameraTransform.localEulerAngles.y,CameraTransform.localEulerAngles.z);

			float forwardBackward=Input.GetAxis("Vertical")*10f;
			float leftRight=Input.GetAxis("Horizontal")*10f;

			playerDirection.y=0;
			playerDirection = new Vector3(leftRight,0,forwardBackward);
			playerDirection = transform.TransformDirection(playerDirection);

			if(Input.GetKey(KeyCode.Space)){
				playerDirection.y+=10f;
			}

        if (Input.GetKey(KeyCode.LeftShift))
            playerDirection.y -= 20f;

		playerCont.Move(playerDirection*Time.deltaTime);
	}
	void Shooting(){
		if(Input.GetKeyDown(KeyCode.Mouse0)){
			GameObject projectile=Instantiate(Projectile,Barrel.transform.position,Quaternion.identity) as GameObject;
			projectile.name="Bullet";
			projectile.GetComponent<Rigidbody>().AddForce(Barrel.transform.forward*5000f);
		}
	}
}
