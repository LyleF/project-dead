﻿using UnityEngine;
using System.Collections;

public class PlayerProjectile : MonoBehaviour {

	public GameObject ForearmGib;
	public GameObject ShoulderGib;
	public GameObject ArmGib;
	public GameObject ChestGib;
	public GameObject LowerLegGib;
	public GameObject UpperLegGib;

	public GameObject BloodSplatter;
	public GameObject BloodParticleReference;
	public GameObject HeadShotReference;

	private GameObject blood;
	private GameObject HeadshotSplatter;
	private GameObject bloodSplatter;

	private GameObject LeftForearm;
	private GameObject LeftShoulder;
	private GameObject LeftArm;
	private GameObject LeftUpperLeg;
	private GameObject LeftLowerLeg;

	private GameObject RightForearm;
	private GameObject RightShoulder;
	private GameObject RightArm;
	private GameObject RightUpperLeg;
	private GameObject RightLowerLeg;

	private GameObject Chest;

	private bool isHit;
	private float BloodSplatterScale;

	private Dismemberment DismemberReference;

    public int damage;
	
	void OnEnable(){
	}

    void Awake()
    {
        Destroy(gameObject, 1.5f);
    }
    
	void OnCollisionEnter(Collision col){
		//collision for enemies
		if(col.gameObject.tag=="Enemy"||col.gameObject.tag==""){
			//raycasting for blood sprites
			RaycastHit bloodRay;
			//cast a ray on the backside of an object
			if(Physics.Raycast(transform.position,-col.transform.forward,out bloodRay,2f)){

				if(bloodRay.collider.tag=="BloodSurface"){
					BloodSplatterScale=Random.Range(0.05f,0.2f);
					bloodSplatter=Instantiate(BloodSplatter,bloodRay.point,Quaternion.FromToRotation(Vector3.up, bloodRay.normal)) as GameObject;
					//change scale of instantiated bloodsplatters in order to add more variety
					bloodSplatter.transform.localScale=new Vector3(BloodSplatterScale,BloodSplatterScale,BloodSplatterScale);
					//increment janitor script in order to delete the blood sprites once it reaches a certain number (varies depending on player settings)
					JanitorScript.CleanEnvironment.BloodSpriteCTR++;
				}
			}
       //     Debug.Log(col.gameObject.name);
        }
        if (col.gameObject.tag != "Player")
            Destroy(gameObject);

        if (col.transform.root.GetComponent<EnemyHP>() != null)
        {
            if (col.transform.root.GetComponent<EnemyHP>().hp <= 0)
            {
                if (col.transform.name == "LeftElbow")
                {
                    col.transform.root.GetComponent<PhotonView>().RPC("dismember", PhotonTargets.AllBuffered, "LBraso");
                    col.transform.root.GetComponent<PhotonView>().RPC("dismember", PhotonTargets.AllBuffered, transform.name);

                    PhotonNetwork.Instantiate("Stalker_LeftForearm",col.transform.position,col.transform.rotation,0,null);
                }
                if (col.transform.name == "RightElbow")
                {
                    col.transform.root.GetComponent<PhotonView>().RPC("dismember", PhotonTargets.AllBuffered, "RBraso");
                    col.transform.root.GetComponent<PhotonView>().RPC("dismember", PhotonTargets.AllBuffered, transform.name);

                    PhotonNetwork.Instantiate("Stalker_RightForearm", col.transform.position, col.transform.rotation, 0, null);
                }
            }
        }
        //Debug.Log(col.gameObject.name);
    }

	void Destroy(){
		GetComponent<Rigidbody>().velocity=Vector3.zero;
		gameObject.SetActive(false);
	}
	void OnDisable(){
		CancelInvoke();
	}
}
