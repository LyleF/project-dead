﻿using UnityEngine;
using System.Collections;

public class PlayerHP : MonoBehaviour {
	public static PlayerHP playerHP;

	private float hp;

	// Use this for initialization
	void Start () {
		playerHP=this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void ModifyHP(bool isHit,int index){

		if(isHit&&index==0){
			hp-=25f;
		}
		if(isHit&&index==1){
			hp-=20f;
		}
	}
	public void resetHP(float hp){ //add max HP for selected perk
		if(hp<=100&&hp>0){
			this.hp=hp;
		}
	}
	public float getHP(){
		return hp;
	}
}
