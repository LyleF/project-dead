﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EnemyHP : Photon.MonoBehaviour {
    private NetworkManager netman;

    private List<GameObject> bodyCheck;

    private Transform bodyParts;
    private Transform rigidBodyJoints;

    private Transform rightLegJoint;
    private Transform leftLegJoint;
    private Transform chestJoint;

    private Transform chestLeftJoint;
    private Transform chestRightJoint;
    private Transform neckJoint;

    private Transform headJoint;

    private bool decrementList;
    private bool isDead;

    internal int hp;
	// Use this for 
    void Awake()
    {
        bodyCheck = new List<GameObject>();
        if (transform.name.Contains("Stalker"))
        {
            hp = 100;
        }

        if (transform.name.Contains("Brute"))
        {
            hp = 250;
        }

        if (transform.name.Contains("Bomber"))
        {
            hp = 80;
        }

        if (transform.name.Contains("Specimen"))
        {
            hp = 2000;
        }
    }

	void Start () {

        if (transform.name.Contains("Stalker"))
        {
            bodyParts = transform.Find("Katawan");
            rigidBodyJoints = transform.Find("MainRoot");
        }

        if (transform.name.Contains("Brute"))
        {
            bodyParts = transform.Find("MyFirstCharacter:RootGrp");
            rigidBodyJoints = transform.Find("MyFirstCharacter:RootGrp");
        }

        netman = FindObjectOfType<NetworkManager>();

      //  checkJoints();
    }

    // Update is called once per frame
    void Update () {

        if (PhotonNetwork.isMasterClient)
        {
            if (hp <= 0)
            {

                GetComponent<Animator>().enabled = false;
                GetComponent<AIBehavior>().enabled = false;
                GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;

                if (!isDead)
                {
                    if (!transform.name.Contains("Bomber"))
                    {
                        isDead = true;
                        photonView.RPC("enableDismemberment", PhotonTargets.All);
                        photonView.RPC("destroyMe", PhotonTargets.All);
                    }
                }
               

                if (!decrementList && GameObject.Find("YourPlayer")!=null)
                {
                    if (transform.name.Contains("Bomber"))
                    {
                        photonView.RPC("fart", PhotonTargets.All);
                        photonView.RPC("quickDestroy", PhotonTargets.All);
                    }
  
                        GameObject.Find("YourPlayer").GetComponent<PhotonView>().RPC("DecrementEnemyCount", PhotonTargets.All);
                        decrementList = true;
                    
                }
            }
        }
    }
    [PunRPC]
    void takingDamage(int damage)
    {
        hp -= damage;
    }
    [PunRPC]
    void destroyMe()
    {
        Destroy(gameObject, 7f);
    }
    [PunRPC]
    void quickDestroy()
    {
        DestroyObject(gameObject);
    }
    [PunRPC]
    public void enableDismemberment()
    {
        GetComponent<Animator>().enabled = false;
        GetComponent<AIBehavior>().enabled = false;
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;

        foreach (Transform jointCheck in rigidBodyJoints)
        {
            jointCheck.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
            jointCheck.GetComponent<Rigidbody>().useGravity = true;
            jointCheck.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");
            foreach (Transform jointCheck1 in jointCheck)
            {
                if (!transform.root.name.Contains("Brute"))
                {
                    jointCheck1.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                    jointCheck1.GetComponent<Rigidbody>().useGravity = true;
                    jointCheck1.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");
                }
                foreach (Transform jointCheck2 in jointCheck1)
                {
                    jointCheck2.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                    jointCheck2.GetComponent<Rigidbody>().useGravity = true;
                    jointCheck2.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");
                    //checking middle joints

                    if (jointCheck2.name == "RightLeg")
                        rightLegJoint = jointCheck2;
                    if (jointCheck2.name == "LeftLeg")
                        leftLegJoint = jointCheck2;
                    if (jointCheck2.name == "Chest")
                        chestJoint = jointCheck2;

                    if (leftLegJoint != null)
                    {
                        foreach (Transform insideLeftLeg in leftLegJoint)
                        {
                            insideLeftLeg.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                            insideLeftLeg.GetComponent<Rigidbody>().useGravity = true;
                            insideLeftLeg.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");
                        }
                    }

                    if (rightLegJoint != null)
                    {
                        foreach (Transform insideRightLeg in rightLegJoint)
                        {
                            insideRightLeg.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                            insideRightLeg.GetComponent<Rigidbody>().useGravity = true;
                            insideRightLeg.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");
                        }
                    }

                    if (chestJoint != null)
                    {
                        //check middle joint
                        foreach (Transform insideMiddle in chestJoint)
                        {
                            insideMiddle.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                            insideMiddle.GetComponent<Rigidbody>().useGravity = true;
                            insideMiddle.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");

                            if (insideMiddle.name == "Neck")
                                neckJoint = insideMiddle;
                            if (insideMiddle.name == "RightArm")
                                chestRightJoint = insideMiddle;
                            if (insideMiddle.name == "LeftArm")
                                chestLeftJoint = insideMiddle;

                            if (chestLeftJoint != null)
                            {
                                foreach (Transform left in chestLeftJoint)
                                {
                                    left.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                                    left.GetComponent<Rigidbody>().useGravity = true;
                                    left.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");
                                }
                            }
                            if (chestRightJoint != null)
                            {
                                foreach (Transform right in chestRightJoint)
                                {
                                    right.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezeAll;
                                    right.GetComponent<Rigidbody>().useGravity = true;
                                    right.gameObject.layer = LayerMask.NameToLayer("DeadEnemies");

                                }
                            }

                        }
                    }
                }
            }
        }
    }

    void checkJoints()
    {
        foreach (Transform jointCheck in rigidBodyJoints)
        {
            bodyCheck.Add(jointCheck.gameObject);
            foreach (Transform jointCheck1 in jointCheck)
            {
                bodyCheck.Add(jointCheck1.gameObject);
                foreach (Transform jointCheck2 in jointCheck1)
                {

                    bodyCheck.Add(jointCheck2.gameObject);
                    //checking middle joints

                    if (jointCheck2.name == "RightLeg")
                        rightLegJoint = jointCheck2;
                    if (jointCheck2.name == "LeftLeg")
                        leftLegJoint = jointCheck2;
                    if (jointCheck2.name == "Chest")
                        chestJoint = jointCheck2;

                    if (leftLegJoint != null)
                    {
                        foreach (Transform insideLeftLeg in leftLegJoint)
                        {
                            bodyCheck.Add(insideLeftLeg.gameObject);
                        }
                    }

                    if (rightLegJoint != null)
                    {
                        foreach (Transform insideRightLeg in rightLegJoint)
                        {
                            bodyCheck.Add(insideRightLeg.gameObject);
                        }
                    }

                    if (chestJoint != null)
                    {
                        //check middle joint
                        foreach (Transform insideMiddle in chestJoint)
                        {
                            bodyCheck.Add(insideMiddle.gameObject);

                            if (insideMiddle.name == "Neck")
                                neckJoint = insideMiddle;
                            if (insideMiddle.name == "RightArm")
                                chestRightJoint = insideMiddle;
                            if (insideMiddle.name == "LeftArm")
                                chestLeftJoint = insideMiddle;

                            if (chestLeftJoint != null)
                            {
                                foreach (Transform left in chestLeftJoint)
                                {
                                    bodyCheck.Add(left.gameObject);
                                }
                            }
                            if (chestRightJoint != null)
                            {
                                foreach (Transform right in chestRightJoint)
                                {
                                    bodyCheck.Add(right.gameObject);
                                }
                            }
                        }
                    }
                }
            }
        }
    } 
}
