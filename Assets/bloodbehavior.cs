﻿using UnityEngine;
using System.Collections;

public class bloodbehavior : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("killMe", PhotonTargets.All);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [PunRPC]
    void killMe()
    {
        Destroy(gameObject, 2f);
    }
}
