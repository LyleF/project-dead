﻿using UnityEngine;
using System.Collections;

public class fartBehavior : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("destroyMe", PhotonTargets.All);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [PunRPC]
    void destroyMe()
    {
        Destroy(gameObject, 5f);
    }
}
